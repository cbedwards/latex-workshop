\begin{thebibliography}{63}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{{\tt #1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi

\bibitem[{Adler et~al.(2010)Adler, Ellner, and Levine}]{Adler:2010jt}
Adler, P.~B., S.~P. Ellner, and J.~M. Levine.
\newblock 2010.
\newblock {Coexistence of perennial plants: an embarrassment of niches.}
\newblock Ecology Letters {\bf 13}:1019--1029.

\bibitem[{Adler et~al.(2006)Adler, Hillerislambers, Kyriakidis, Guan, and
  Levine}]{Adler:2006ewa}
Adler, P.~B., J.~Hillerislambers, P.~C. Kyriakidis, Q.~Guan, and J.~M. Levine.
\newblock 2006.
\newblock {Climate variability has a stabilizing effect on the coexistence of
  prairie grasses.}
\newblock Proceedings of the National Academy of Sciences {\bf
  103}:12793--12798.

\bibitem[{Adler et~al.(2007)Adler, Hillerislambers, and Levine}]{Adler:2007ef}
Adler, P.~B., J.~Hillerislambers, and J.~M. Levine.
\newblock 2007.
\newblock {A niche for neutrality.}
\newblock Ecology Letters {\bf 10}:95--104.

\bibitem[{Agrawal et~al.(2007)Agrawal, Ackerly, and Adler}]{Agrawal:2007el}
Agrawal, A.~A., D.~D. Ackerly, and F.~Adler.
\newblock 2007.
\newblock {Filling key gaps in population and community ecology}.
\newblock Frontiers in Ecology and the Environment {\bf 5}:145--152.

\bibitem[{Amarasekare(2003)}]{Amarasekare:2003cka}
Amarasekare, P.
\newblock 2003.
\newblock {Competitive coexistence in spatially structured environments: a
  synthesis}.
\newblock Ecology Letters {\bf 6}:1109--1122.

\bibitem[{Angert et~al.(2009)Angert, Huxman, Chesson, and
  Venable}]{Angert:2009hba}
Angert, A.~L., T.~E. Huxman, P.~Chesson, and D.~L. Venable.
\newblock 2009.
\newblock {Functional tradeoffs determine species coexistence via the storage
  effect.}
\newblock Proceedings of the National Academy of Sciences of the United States
  of America {\bf 106}:11641--11645.

\bibitem[{Arceo-G{\'o}mez et~al.(2015)Arceo-G{\'o}mez, Abdala-Roberts,
  Jankowiak, Kohler, Meindl, Navarro-Fern{\'a}ndez, Parra-Tabla, Ashman, and
  Alonso}]{ArceoGomez:2015fd}
Arceo-G{\'o}mez, G., L.~Abdala-Roberts, A.~Jankowiak, C.~Kohler, G.~A. Meindl,
  C.~M. Navarro-Fern{\'a}ndez, V.~Parra-Tabla, T.-L. Ashman, and C.~Alonso.
\newblock 2015.
\newblock {Patterns of among- and within-species variation in heterospecific
  pollen receipt: The importance of ecological generalization.}
\newblock American Journal of Botany {\bf 103}:1--12.

\bibitem[{Arceo-G{\'o}mez and Ashman(2014)}]{ArceoGomez:2014go}
Arceo-G{\'o}mez, G., and T.-L. Ashman.
\newblock 2014.
\newblock {Coflowering community context influences female fitness and alters
  the adaptive value of flower longevity in Mimulus guttatus.}
\newblock The American Naturalist {\bf 183}:E50--63.

\bibitem[{Bateman(1951)}]{Bateman:1951kw}
Bateman, a.~J.
\newblock 1951.
\newblock {The taxonomic discrimination of bees}.
\newblock Heredity.

\bibitem[{Benadi(2015)}]{Benadi:2015km}
Benadi, G.
\newblock 2015.
\newblock {Requirements for plant coexistence through pollination niche
  partitioning}.
\newblock Proceedings of the Royal Society of London B: Biological Sciences
  {\bf 282}.

\bibitem[{Bengtsson et~al.(1994)Bengtsson, Fagerstr{\"o}m, and
  Rydin}]{Bengtsson:1994ksa}
Bengtsson, J., T.~Fagerstr{\"o}m, and H.~Rydin.
\newblock 1994.
\newblock {Competition and coexistence in plant communities}.
\newblock Trends in Ecology {\&} Evolution {\bf 9}:246--250.

\bibitem[{Briscoe~Runquist(2012)}]{BriscoeRunquist:2012bf}
Briscoe~Runquist, R.~D.
\newblock 2012.
\newblock {Pollinator-mediated competition between two congeners, Limnanthes
  douglasii subsp. rosea and L. alba (Limnanthaceae).}
\newblock American Journal of Botany {\bf 99}:1125--1132.

\bibitem[{Cane and Sipes(2006)}]{CaneSipes:006YePOz}
Cane, J.~H., and S.~D. Sipes, 2006.
\newblock {Characterizing Floral Specialization by Bees: Analytical Methods and
  a Revised Lexicon for Oligolecty}.
\newblock Pages 1--26 {\em in\/} N.~M. Waser and J.~Ollerton, editors. Plant
  Pollinator Interactions. University of Chicago Press.

\bibitem[{Chase and Leibold(2003)}]{Chase:2003ui}
Chase, J., and M.~Leibold.
\newblock 2003.
\newblock {Ecological Niches: Linking Classical and Contemporary Approaches}.
\newblock University of Chicago Press, Chicago.

\bibitem[{Chesson(2000{\natexlab{{\em a\/}}})}]{Chesson:2000kt}
Chesson, P.
\newblock 2000{\natexlab{{\em a\/}}}.
\newblock {General Theory of Competitive Coexistence in Spatially-Varying
  Environments}.
\newblock Theoretical Population Biology {\bf 58}:211--237.

\bibitem[{Chesson(2000{\natexlab{{\em b\/}}})}]{Chesson:2000gl}
Chesson, P.
\newblock 2000{\natexlab{{\em b\/}}}.
\newblock {Mechanisms of maintenance of species diversity}.
\newblock Annual Review of Ecology and Systematics {\bf 31}:343--358.

\bibitem[{Chesson and Huntly(1997)}]{Chesson:1997ge}
Chesson, P., and N.~Huntly.
\newblock 1997.
\newblock {The roles of harsh and fluctuating conditions in the dynamics of
  ecological communities.}
\newblock The American Naturalist {\bf 150}:519--553.

\bibitem[{Chesson and Kuang(2008)}]{Chesson:2008id}
Chesson, P., and J.~J. Kuang.
\newblock 2008.
\newblock {The interaction between predation and competition}.
\newblock Nature {\bf 456}:235--238.

\bibitem[{Chesson and Kuang(2010)}]{Chesson:2010bi}
Chesson, P., and J.~J. Kuang.
\newblock 2010.
\newblock {The storage effect due to frequency-dependent predation in
  multispecies plant communities.}
\newblock Theoretical Population Biology {\bf 78}:148--164.

\bibitem[{Chu and Adler(2015)}]{Chu:2015dk}
Chu, C., and P.~B. Adler.
\newblock 2015.
\newblock {Large niche differences emerge at the recruitment stage to stabilize
  grassland coexistence}.
\newblock Ecological Monographs {\bf 85}:373--392.

\bibitem[{Drescher et~al.(2014)Drescher, Wallace, Katouli, Massaro, and
  Leonhardt}]{Drescher:2014if}
Drescher, N., H.~M. Wallace, M.~Katouli, C.~F. Massaro, and S.~D. Leonhardt.
\newblock 2014.
\newblock {Diversity matters: how bees benefit from different resin sources}.
\newblock Oecologia {\bf 176}:943--953.

\bibitem[{Dybzinski and Tilman(2007)}]{Dybzinski:2007hh}
Dybzinski, R., and D.~Tilman.
\newblock 2007.
\newblock {Resource use patterns predict long-term outcomes of plant
  competition for nutrients and light.}
\newblock The American Naturalist {\bf 170}:305--318.

\bibitem[{Eckhart et~al.(2013)Eckhart, Jennison, Kircher, and
  Montgomery}]{Eckhart:2013tl}
Eckhart, V.~M., K.~Jennison, B.~Kircher, and D.~M. Montgomery, 2013.
\newblock {Where must niches differ, if differ they must? Spatial scaling of
  climatic, topographic, and soil control of distribution in closely related
  annual plants}.
\newblock  {\em in\/} Poster.

\bibitem[{Essenberg(2012)}]{Essenberg:2012jc}
Essenberg, C.~J.
\newblock 2012.
\newblock {Explaining variation in the effect of floral density on pollinator
  visitation.}
\newblock The American Naturalist {\bf 180}:153--166.

\bibitem[{Feldman et~al.(2004)Feldman, Morris, and Wilson}]{Feldman:2004wu}
Feldman, T.~S., W.~F. Morris, and W.~G. Wilson.
\newblock 2004.
\newblock {When can two plant species facilitate each other's pollination?}
\newblock Oikos {\bf 105}:197--207.

\bibitem[{Fortner and Weltzin(2007)}]{Fortner:2009by}
Fortner, A.~M., and J.~F. Weltzin.
\newblock 2007.
\newblock {Competitive hierarchy for four common old-field plant species
  depends on resource identity and availability}.
\newblock The Journal of the Torrey Botanical Society {\bf 134}:166--176.

\bibitem[{Geber and Moeller(2006)}]{Geber:2006wx}
Geber, M.~A., and D.~A. Moeller, 2006.
\newblock {Pollinator responses to plant communities and implications for
  reproductive character evolution}.
\newblock Pages 102--119 {\em in\/} L.~D. Harder and S.~C.~H. Barrett, editors.
  Ecology And Evolution Of Flowers. Oxford University Press.

\bibitem[{Ghazoul(2006)}]{Ghazoul:2006ip}
Ghazoul, J.
\newblock 2006.
\newblock {Floral diversity and the facilitation of pollination}.
\newblock Journal of Ecology {\bf 94}:295--304.

\bibitem[{Godoy et~al.(2014)Godoy, Kraft, and Levine}]{Godoy:2014cx}
Godoy, O., N.~J.~B. Kraft, and J.~M. Levine.
\newblock 2014.
\newblock {Phylogenetic relatedness and the determinants of competitive
  outcomes.}
\newblock Ecology Letters {\bf 17}:836--844.

\bibitem[{Godoy and Levine(2014)}]{Godoy:2014io}
Godoy, O., and J.~M. Levine.
\newblock 2014.
\newblock {Phenology effects on invasion success: insights from coupling field
  experiments to coexistence theory}.
\newblock Ecology {\bf 95}:726--736.

\bibitem[{HilleRisLambers et~al.(2012)HilleRisLambers, Adler, Harpole, Levine,
  and Mayfield}]{HilleRisLambers:2012jvc}
HilleRisLambers, J., P.~B. Adler, W.~S. Harpole, J.~M. Levine, and M.~M.
  Mayfield.
\newblock 2012.
\newblock {Rethinking Community Assembly through the Lens of Coexistence
  Theory}.
\newblock Annual Review of Ecology, Evolution, and Systematics {\bf
  43}:227--248.

\bibitem[{Howland(2015)}]{Howland:2015vp}
Howland, M., 2015.
\newblock {Contrasting responses to soil properties may enable coexistence of
  closely related annual plants}.
\newblock  {\em in\/} Poster.

\bibitem[{Janovsky et~al.(2013)Janovsky, Mikat, Hadrava, Horcickova, Kmecova,
  Pozarova, Smycka, and Herben}]{Janovsky:2013kfa}
Janovsky, Z., M.~Mikat, J.~Hadrava, E.~Horcickova, K.~Kmecova, D.~Pozarova,
  J.~Smycka, and T.~Herben.
\newblock 2013.
\newblock {Conspecific and Heterospecific Plant Densities at Small-Scale Can
  Drive Plant-Pollinator Interactions}.
\newblock PLoS ONE {\bf 8}.

\bibitem[{Kipling and Warren(2014)}]{Kipling:2014ix}
Kipling, R.~P., and J.~Warren.
\newblock 2014.
\newblock {How generalists coexist: the role of floral phenotype and spatial
  factors in the pollination systems of two Ranunculus species}.
\newblock Journal of Plant Ecology {\bf 7}:480--489.

\bibitem[{Kraft et~al.(2015)Kraft, Godoy, and Levine}]{Kraft:2015eda}
Kraft, N. J.~B., O.~Godoy, and J.~M. Levine.
\newblock 2015.
\newblock {Plant functional traits and the multidimensional nature of species
  coexistence.}
\newblock Proceedings of the National Academy of Sciences of the United States
  of America {\bf 112}:797--802.

\bibitem[{Kuang and Chesson(2010)}]{Kuang:2010fx}
Kuang, J.~J., and P.~Chesson.
\newblock 2010.
\newblock {Interacting coexistence mechanisms in annual plant communities:
  Frequency-dependent predation and the storage effect.}
\newblock Theoretical Population Biology {\bf 77}:56--70.

\bibitem[{Kunin and Iwasa(1996)}]{Kunin:1996vo}
Kunin, W., and Y.~Iwasa.
\newblock 1996.
\newblock {Pollinator Foraging Strategies in Mixed Floral Arrays: Density
  Effects and Floral Constancy}.
\newblock Theoretical Population Biology {\bf 49}:232--263.

\bibitem[{Kunin(1997)}]{Kunin:1997if}
Kunin, W.~E.
\newblock 1997.
\newblock {Population Size and Density Effects in Pollination: Pollinator
  Foraging and Plant Reproductive Success in Experimental Arrays of Brassica
  Kaber}.
\newblock The Journal of Ecology {\bf 85}:225.

\bibitem[{Levine and Hillerislambers(2009)}]{Levine:2009doa}
Levine, J.~M., and J.~Hillerislambers.
\newblock 2009.
\newblock {The importance of niches for the maintenance of species diversity}.
\newblock Nature {\bf 461}:254--257.

\bibitem[{Lewis and Lewis(1955)}]{Lewis:1955uh}
Lewis, H., and M.~E. Lewis, 1955.
\newblock {The genus Clarkia}.

\bibitem[{Liao et~al.(2011)Liao, Gituru, Guo, and Wang}]{Liao:2011gp}
Liao, K., R.~W. Gituru, Y.-H. Guo, and Q.-F. Wang.
\newblock 2011.
\newblock {The presence of co-flowering species facilitates reproductive
  success of Pedicularis monbeigiana (Orobanchaceae) through variation in
  bumble-bee foraging behaviour.}
\newblock Annals of Botany {\bf 108}:877--884.

\bibitem[{Michener(2007)}]{Michener:2007ve}
Michener, C.~D.
\newblock 2007.
\newblock {The Bees of the World}.
\newblock Second edition edition.
\newblock The Johns Hopkins University Press, Baltimore.

\bibitem[{Moeller(2004)}]{Moeller:2004ck}
Moeller, D.~A.
\newblock 2004.
\newblock {Facilitative interactions among plants via shared pollinators}.
\newblock Ecology {\bf 85}:3289--3301.

\bibitem[{Moeller and Geber(2005)}]{Moeller:2005ej}
Moeller, D.~A., and M.~A. Geber.
\newblock 2005.
\newblock {Ecological Context of the Evolution of Self-Pollination in Clarkia
  Xantlana: Poulation Size, Plant Communities, and Reproductive Assurance}.
\newblock Evolution {\bf 59}:786--799.

\bibitem[{Morales and Traveset(2008)}]{Morales:2008dw}
Morales, C.~L., and A.~Traveset.
\newblock 2008.
\newblock {Interspecific Pollen Transfer: Magnitude, Prevalence and
  Consequences for Plant Fitness}.
\newblock Critical Reviews in Plant Sciences {\bf 27}:221--238.

\bibitem[{Pauw(2013)}]{Pauw:2013kl}
Pauw, A.
\newblock 2013.
\newblock {Can pollination niches facilitate plant coexistence?}
\newblock Trends in Ecology {\&} Evolution {\bf 28}:30--37.

\bibitem[{Potts et~al.(2006)Potts, Petanidou, Roberts, O{\textquoteright}Toole,
  Hulbert, and Willmer}]{Potts:2006cr}
Potts, S.~G., T.~Petanidou, S.~Roberts, C.~O{\textquoteright}Toole, A.~Hulbert,
  and P.~Willmer.
\newblock 2006.
\newblock {Plant-pollinator biodiversity and pollination services in a complex
  Mediterranean landscape}.
\newblock Biological Conservation {\bf 129}:519--529.

\bibitem[{Robertson(1895)}]{Robertson:1895bs}
Robertson, C.
\newblock 1895.
\newblock {The Philosophy of Flower Seasons, and the Phaenological Relations of
  the Entomophilous Flora and the Anthophilous Insect Fauna}.
\newblock American Naturalist {\bf 29}:97--117.

\bibitem[{Schmitt(1983)}]{Schmitt:1983gt}
Schmitt, J.
\newblock 1983.
\newblock {Flowering plant density and pollinator visitation in Senecio}.
\newblock Oecologia {\bf 60}:97--102.

\bibitem[{Siepielski and McPeek(2010)}]{Siepielski:2010jp}
Siepielski, A.~M., and M.~a. McPeek.
\newblock 2010.
\newblock {On the evidence for species coexistence: a critique of the
  coexistence program.}
\newblock Ecology {\bf 91}:3153--3164.

\bibitem[{Singh(2013)}]{IndraniCornellUniversitySingh:2S0icDGp}
Singh, I. C.~U.
\newblock 2013.
\newblock {Master's Thesis} .

\bibitem[{Soliveres et~al.(2015)Soliveres, Maestre, Ulrich, Manning, Boch,
  Bowker, Prati, Delgado-Baquerizo, Quero, Sch{\"o}ning, Gallardo, Weisser,
  M{\"u}ller, Socher, Garc{\'\i}a-G{\'o}mez, Ochoa, Schulze, Fischer, and
  Allan}]{Soliveres:2015iz}
Soliveres, S., F.~T. Maestre, W.~Ulrich, P.~Manning, S.~Boch, M.~A. Bowker,
  D.~Prati, M.~Delgado-Baquerizo, J.~L. Quero, I.~Sch{\"o}ning, A.~Gallardo,
  W.~Weisser, J.~M{\"u}ller, S.~A. Socher, M.~Garc{\'\i}a-G{\'o}mez, V.~Ochoa,
  E.-D. Schulze, M.~Fischer, and E.~Allan.
\newblock 2015.
\newblock {Intransitive competition is widespread in plant communities and
  maintains their species richness.}
\newblock Ecology Letters {\bf 18}:790--798.

\bibitem[{{Sommer, U} and {Worm, B}(2002)}]{Sommer:2002iz}
{Sommer, U}, and {Worm, B}.
\newblock 2002.
\newblock {Competition and coexistence}.
\newblock Springer-Verlag Berlin Heidelberg, New York.

\bibitem[{Sowig(1989)}]{Sowig:1989fu}
Sowig, P.
\newblock 1989.
\newblock {Effects of flowering plant's patch size on species composition of
  pollinator communities, foraging strategies, and resource partitioning in
  bumblebees (Hymenoptera: Apidae)}.
\newblock Oecologia {\bf 78}:550--558.

\bibitem[{Stoll and Prati(2001)}]{Stoll:2008jb}
Stoll, P., and D.~Prati.
\newblock 2001.
\newblock {Intraspecific aggregation alters competitive interactions in
  experimental plant communities}.
\newblock Ecology {\bf 82}:319--327.

\bibitem[{Sytsma et~al.(1990)Sytsma, Smith, and Gottlieb}]{Sytsma:1990fi}
Sytsma, K.~J., J.~F. Smith, and L.~D. Gottlieb.
\newblock 1990.
\newblock {Phylogenetics in Clarkia (Onagraceae): Restriction Site Mapping of
  Chloroplast DNA}.
\newblock Systematic Botany {\bf 15}:280--295.

\bibitem[{Tubay et~al.(2015)Tubay, Suzuki, Uehara, Kakishima, Ito, Ishida,
  Yoshida, Mori, Rabajante, Morita, YOKOZAWA, and Yoshimura}]{Tubay:2015bd}
Tubay, J.~M., K.~Suzuki, T.~Uehara, S.~Kakishima, H.~Ito, A.~Ishida,
  K.~Yoshida, S.~Mori, J.~F. Rabajante, S.~Morita, M.~YOKOZAWA, and
  J.~Yoshimura.
\newblock 2015.
\newblock {Microhabitat locality allows multi-species coexistence in
  terrestrial plant communities.}
\newblock Scientific reports {\bf 5}:15376.

\bibitem[{Vamosi et~al.(2006)Vamosi, Knight, Steets, Mazer, Burd, and
  Ashman}]{Vamosi:2006wj}
Vamosi, J.~C., T.~M. Knight, J.~A. Steets, S.~J. Mazer, M.~Burd, and T.-L.
  Ashman.
\newblock 2006.
\newblock {Pollination decays in biodiversity hotspots.}
\newblock Proceedings of the National Academy of Sciences {\bf 103}:956--961.

\bibitem[{Wagg et~al.(2011)Wagg, Jansa, Stadler, Schmid, and Van
  Der~Heijden}]{Wagg:2011vr}
Wagg, C., J.~Jansa, M.~Stadler, B.~Schmid, and M.~G.~A. Van Der~Heijden.
\newblock 2011.
\newblock {Mycorrhizal fungal identity and diversity relaxes plant-plant
  competition.}
\newblock Ecology {\bf 92}:1303--1313.

\bibitem[{Waser(1986)}]{Waser:1986et}
Waser, N.~M.
\newblock 1986.
\newblock {Flower constancy: definition, cause, and measurement}.
\newblock American Naturalist {\bf 127}:593--603.

\bibitem[{Waser(1998)}]{Waser:1998bfa}
Waser, N.~M.
\newblock 1998.
\newblock {Pollination, angiosperm speciation, and the nature of species
  boundaries}.
\newblock Oikos {\bf 82}:198.

\bibitem[{Waser and Campbell(2004)}]{Waser:2004ju}
Waser, N.~M., and D.~R. Campbell, 2004.
\newblock {Ecological Speciation in Flowering Plants }.
\newblock Pages 264--277 {\em in\/} U.~Dieckmann, M.~Doebeli, J.~A.~J. Metz,
  D.~Tautz, and D.~Schluter, editors. Adaptive Speciation. The ecology and
  evolution of flowers, Cambridge.

\bibitem[{Waser et~al.(2008)Waser, Chittka, Price, Williams, and
  Ollerton}]{Waser:2008be}
Waser, N.~M., L.~Chittka, M.~V. Price, N.~M. Williams, and J.~Ollerton.
\newblock 2008.
\newblock {Generalization in Pollination Systems, and Why it Matters}.
\newblock Ecology {\bf 77}:1043.

\end{thebibliography}
