\begin{thebibliography}{60}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{{\tt #1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi

\bibitem[{Allen et~al.(2011)Allen, Zwaan, and Brakefield}]{Allen11}
Allen, C.~E., B.~J. Zwaan, and P.~M. Brakefield.
\newblock 2011.
\newblock {Evolution of Sexual Dimorphism in the Lepidoptera}.
\newblock Annual Review of Entomology {\bf 56}:445--464.
\newblock \urlprefix\url{http://dx.doi.org/10.1146/annurev-ento-120709-144828}.

\bibitem[{Almbro and Kullberg(2012)}]{Almbro12}
Almbro, M., and C.~Kullberg.
\newblock 2012.
\newblock {Weight Loading and Reproductive Status Affect the Flight Performance
  of Pieris napi Butterflies}.
\newblock Journal of Insect Behavior {\bf 25}:441--452.
\newblock \urlprefix\url{http://dx.doi.org/10.1007/s10905-011-9309-1}.

\bibitem[{Angelo and Slansky~Jr.(1984)}]{angelo84}
Angelo, M., and F.~Slansky~Jr.
\newblock 1984.
\newblock Body Building Insects: Trade-Offs in Resource Allocation with
  Particular Reference to Migratory Species.
\newblock Florida Entomologist {\bf 67}:22--42.

\bibitem[{Bates et~al.(2013)Bates, Maechler, and Bolker}]{lmer}
Bates, D., M.~Maechler, and B.~Bolker, 2013.
\newblock lme4: Linear mixed-effects models using S4 classes.
\newblock \urlprefix\url{http://CRAN.R-project.org/package=lme4}.

\bibitem[{Beehler and Foster(1988)}]{beehler88}
Beehler, B.~M., and M.~S. Foster.
\newblock 1988.
\newblock Hotshots, Hotspots, and Female Preference in the Organization of Lek
  Mating Systems.
\newblock The American Naturalist {\bf 131}:203--219.

\bibitem[{Berwaerts et~al.(2002)Berwaerts, Dyck, and Aerts}]{paegeria02}
Berwaerts, K., H.~V. Dyck, and P.~Aerts.
\newblock 2002.
\newblock {Does flight morphology relate to flight performance? An experimental
  test with the butterfly Pararge aegeria}.
\newblock Functional Ecology {\bf 16}:484--491.

\bibitem[{Boggs and Freeman(2005)}]{Boggs05}
Boggs, C.~L., and K.~D. Freeman.
\newblock 2005.
\newblock {Larval food limitation in butterflies: effects on adult resource
  allocation and fitness}.
\newblock Oecologia {\bf 144}:353--361.
\newblock \urlprefix\url{http://dx.doi.org/10.1007/s00442-005-0076-6}.

\bibitem[{Boughton(1998)}]{boughton1998}
Boughton, D.~A., 1998.
\newblock Behavioral and Ecological Mechanisms of Colonization in a
  Metapopulation of the Butterfly Euphydryas editha.
\newblock Ph.D. thesis, The University of Texas at Austin.

\bibitem[{Bowers(1980)}]{bowers80}
Bowers, M.~D.
\newblock 1980.
\newblock {Unpalatability as a Defense Strategy of Euphydryas phaeton
  (Lepidoptera: Nymphalidae)}.
\newblock Evolution {\bf 34}:586--600.

\bibitem[{Bowers(1984)}]{bowers84}
Bowers, M.~D.
\newblock 1984.
\newblock Iridoid glycosides and host-plant specificity in larvae of the
  buckeye butterfly, Junonia coenia (Nymphalidae).
\newblock Journal of Chemical Ecology {\bf 10}:1567--1577.

\bibitem[{Bowers and Stamp(1992)}]{bowerschem}
Bowers, M.~D., and N.~E. Stamp.
\newblock 1992.
\newblock Chemical variation within and between individuals of Plant lanceolata
  (Plantaginaceae).
\newblock Journal of Chemical Ecology {\bf 18}:985--995.

\bibitem[{Bowers et~al.(1992{\natexlab{{\em a\/}}})Bowers, Stamp, and
  Collinge}]{Bowers1992}
Bowers, M.~D., N.~E. Stamp, and S.~K. Collinge.
\newblock 1992{\natexlab{{\em a\/}}}.
\newblock {Early Stage of Host Range Expansion by a Specialist Herbivore,
  Euphydryas phaeton (Nymphalidae)}.
\newblock Ecology {\bf 73}:526--536.

\bibitem[{Bowers et~al.(1992{\natexlab{{\em b\/}}})Bowers, Stamp, and
  Collinge}]{Bowersrange}
Bowers, M.~D., N.~E. Stamp, and S.~K. Collinge.
\newblock 1992{\natexlab{{\em b\/}}}.
\newblock Early stage of host range expansion by a specialist herbivore,
  Euphydryas phaeton (Nymphalidae).
\newblock Ecology {\bf 73}:526--536.

\bibitem[{Bradbury and Gibson(1983)}]{bradbury1983}
Bradbury, J.~W., and R.~M. Gibson, 1983.
\newblock Mate Choice, Chapter leks and mate choice, pages 109--136 .
\newblock Cambridge University Press.

\bibitem[{Carroll et~al.(1997)Carroll, Dingle, and Klassen}]{Carroll:1997fx}
Carroll, S.~P., H.~Dingle, and S.~P. Klassen.
\newblock 1997.
\newblock {Genetic Differentiation of Fitness-Associated Traits Among Rapidly
  Evolving Populations of the Soapberry Bug}.
\newblock Evolution {\bf 51}:1182.

\bibitem[{Cavers et~al.(1980)Cavers, Bassett, and Crompton}]{cavers1980}
Cavers, P., I.~Bassett, and C.~Crompton.
\newblock 1980.
\newblock The Biology of Canadian Weeds.: 47. Plantago lanceolata L.
\newblock Canadian Journal of Plant Science {\bf 60}:1269--1282.

\bibitem[{Danby(1890)}]{danby1890}
Danby, W.
\newblock 1890.
\newblock The food plant of Melitaea taylori.
\newblock Edw. Can Entomol {\bf 22}:121--122.

\bibitem[{Dorchin et~al.(2009)Dorchin, Scott, Clarkin, Luongo, Jordan, and
  Abrahamson}]{DORCHIN:2009cb}
Dorchin, N., E.~R. Scott, C.~E. Clarkin, M.~P. Luongo, S.~Jordan, and W.~G.
  Abrahamson.
\newblock 2009.
\newblock {Behavioural, ecological and genetic evidence confirm the occurrence
  of host-associated differentiation in goldenrod gall-midges}.
\newblock Journal of Evolutionary Biology {\bf 22}:729--739.

\bibitem[{Dr\`es and Mallet(2002)}]{dres02}
Dr\`es, M., and J.~Mallet.
\newblock 2002.
\newblock Host Races in Plant-Feeding Insects and Their Importance in Sympatric
  Speciation.
\newblock Philosophical Transactions: Biological Sciences {\bf 357}:471--492.

\bibitem[{Dudley(2002)}]{dudley02}
Dudley, R., 2002.
\newblock The Biomechanics of Insect Flight: Form, Function, Evolution,
  Chapter~7 .
\newblock Princeton University Press.

\bibitem[{Ehrlich(1992)}]{ehrlich1992}
Ehrlich, P.~R.
\newblock 1992.
\newblock Population biology of checkerspot butterflies and the preservation of
  global biodiversity.
\newblock Oikos {\bf 63}:6--12.

\bibitem[{Ehrlich and Hanski(2004)}]{ehrlich2004}
Ehrlich, P.~R., and I.~Hanski.
\newblock 2004.
\newblock On the Wings of Checkerspots.
\newblock Oxford University Press.

\bibitem[{Emelianov et~al.(2001)Emelianov, Dr\`es, Baltensweiler, and
  Mallet}]{Emelianov:2001cu}
Emelianov, I., M.~Dr\`es, W.~Baltensweiler, and J.~Mallet.
\newblock 2001.
\newblock {Host-Induced Assortative Mating In Host Races Of The Larch Budmoth}.
\newblock Evolution {\bf 55}:2002--2010.

\bibitem[{Feder et~al.(1988)Feder, Chilcote, and Bush}]{feder88}
Feder, J., C.~Chilcote, and G.~Bush.
\newblock 1988.
\newblock {Genetic differentiation between sympatric host races of the apple
  magot fly Rhagoletis pomella}.
\newblock Nature {\bf 336}:61--64.

\bibitem[{Forister and Scholl(2012)}]{forister2012}
Forister, M.~L., and C.~F. Scholl.
\newblock 2012.
\newblock Use of an Exotic Host Plant Affects Mate Choice in an Insect
  Herbivore.
\newblock The American Naturalist {\bf 179}:805--810.

\bibitem[{Gibbs et~al.(2012)Gibbs, {Van Dyck}, and Breuker}]{Gibbs12}
Gibbs, M., H.~{Van Dyck}, and C.~J. Breuker.
\newblock 2012.
\newblock {Development on drought-stressed host plants affects life history,
  flight morphology and reproductive output relative to landscape structure}.
\newblock Evolutionary Applications {\bf 5}:66--75.
\newblock \urlprefix\url{http://dx.doi.org/10.1111/j.1752-4571.2011.00209.x}.

\bibitem[{Goverde et~al.(2008)Goverde, Bazin, K�ry, ShykoV, and
  Erhardt}]{goverde2008}
Goverde, M., A.~Bazin, M.~K�ry, J.~A. ShykoV, and A.~Erhardt.
\newblock 2008.
\newblock Positive effects of cyanogenic glycosides in food plants on larval
  development of the common blue butterfly.
\newblock Oecologia {\bf 157}:409--418.

\bibitem[{Hammerstein(1982)}]{hammerstein82}
Hammerstein, P.
\newblock 1982.
\newblock The asymmetric war of attrition.
\newblock Journal of Theoretical Biology {\bf 96}:647--682.

\bibitem[{Harrison(1989)}]{harrison1989}
Harrison, S.
\newblock 1989.
\newblock Long-Distance Dispersal and Colonization in the Bay Checkerspot
  Butterfly, Euphydryas Editha Bayensis.
\newblock Ecology {\bf 70}:1236--1243.

\bibitem[{Imafuku and Ohtani(2006)}]{imafuku06}
Imafuku, M., and T.~Ohtani.
\newblock 2006.
\newblock Analysis of coordinated circling and linear flight of a lycaenid
  butterfly species.
\newblock Naturwissenschaften {\bf 93}:131--135.

\bibitem[{Joyce and Pullin(2003)}]{Joyce2003}
Joyce, D.~A., and A.~S. Pullin.
\newblock 2003.
\newblock Conservation implications of the distribution of genetic diversity at
  different scales: a case study using the marsh fritillary butterfly
  (Euphydryas aurinia).
\newblock Biological Conservation {\bf 114}:453--461.
\newblock \urlprefix\url{http://dx.doi.org/10.1016/S0006-3207(03)00087-9}.

\bibitem[{Kemp(2010)}]{Kemp10}
Kemp, D.~J.
\newblock 2010.
\newblock {Sexual selection and morphological design: the tale of two
  territorial butterflies}.
\newblock Australian Journal of Zoology {\bf 58}:289.
\newblock \urlprefix\url{http://dx.doi.org/10.1071/ZO10060}.

\bibitem[{Kemp and Wiklund(2001)}]{kemp01}
Kemp, D.~J., and C.~Wiklund.
\newblock 2001.
\newblock Fighting without Weaponry: A Review of Male-Male Contest Competition
  in Butterflies.
\newblock Behavioral Ecolgoy and Sociobiology {\bf 49}:429--442.

\bibitem[{Kingsolver(1999)}]{kingsolver99}
Kingsolver, J.~G.
\newblock 1999.
\newblock {Experimental Analyses of Wing Size, Flight, and Survival in the
  Western White Butterfly}.
\newblock Evolution {\bf 53}:1479--1490.

\bibitem[{Knuttel and Fielder(2001)}]{knuttel2001}
Knuttel, H., and K.~Fielder.
\newblock 2001.
\newblock Host-Plant-Derived Variation in Ultraviolet Wing Patterns Influences
  Mate Selection by Male Butterflies.
\newblock The Journal of Experimental Biology {\bf 204}:2447--2459.

\bibitem[{Martel et~al.(2003)Martel, R\`{e}jasse~A, Bethenod, and
  Bourguet}]{martel03}
Martel, C., F.~R\`{e}jasse~A, and�Rousset, M.~Bethenod, and D.~Bourguet.
\newblock 2003.
\newblock {Host-plant-associated genetic differentiation in Northern French
  populations of the European corn borer.}
\newblock Heredity {\bf 90}:141--149.

\bibitem[{McLaughlin et~al.(2002)McLaughlin, Hellmann, Boggs, and
  Ehrlich}]{laughlin2002}
McLaughlin, J.~F., J.~J. Hellmann, C.~L. Boggs, and P.~R. Ehrlich.
\newblock 2002.
\newblock The Route and to Extinction and Population Dynamics and of a
  Threatened and Butterfly.
\newblock Oecologia {\bf 132}:538--548.

\bibitem[{Mozaffarian et~al.(2007)Mozaffarian, Sarafrazi, and
  Ganbalani}]{Mozaffarian07}
Mozaffarian, F., A.~Sarafrazi, and G.~N. Ganbalani.
\newblock 2007.
\newblock {Host Plant-Associated Population Variation in the Carob Moth
  Ectomyelois ceratoniae in Iran: A Geometric Morphometric Analysis Suggests a
  Nutritional Basis.}
\newblock Journal of Insect Science {\bf 7}:1--11.
\newblock \urlprefix\url{http://dx.doi.org/10.1673/031.007.0201}.

\bibitem[{Munguira et~al.(1997)Munguira, Martin, Garcia-Barros, and
  Viejo}]{munguira1997}
Munguira, M.~L., J.~Martin, E.~Garcia-Barros, and J.~L. Viejo.
\newblock 1997.
\newblock Use of space and rresource in a Mediterration population of the
  butterfly Euphydryas aurinia.
\newblock Acta OEcologica {\bf 18}:597 -- 612.

\bibitem[{Nosil(2007)}]{Nosil:2007cj}
Nosil, P.
\newblock 2007.
\newblock {Divergent Host Plant Adaptation and Reproductive Isolation between
  Ecotypes of Timema cristinae Walking Sticks}.
\newblock The American Naturalist {\bf 169}:151--162.

\bibitem[{Pashley(1986)}]{pashley86}
Pashley, D.
\newblock 1986.
\newblock Host-associated Genetic Differentiation in Fall Armyworm
  (Lepidoptera: Noctuidae): a Sibling Species Complex.
\newblock Annals of the Entomological Society of America {\bf 79}:898--904.

\bibitem[{Pellegroms et~al.(2009)Pellegroms, {van Dongen}, {van Dyck}, and
  Lens}]{pellengroms09}
Pellegroms, B., S.~{van Dongen}, H.~{van Dyck}, and L.~Lens.
\newblock 2009.
\newblock {Larval food stress differentially affects flight morphology in male
  and female speckled woods ( Pararge aegeria )}.
\newblock Ecological Entomology {\bf 34}:387--393.
\newblock \urlprefix\url{http://dx.doi.org/10.1111/j.1365-2311.2009.01090.x}.

\bibitem[{{R Core Team}(2014)}]{R}
{R Core Team}, 2014.
\newblock R: A Language and Environment for Statistical Computing.
\newblock R Foundation for Statistical Computing, Vienna, Austria.
\newblock \urlprefix\url{http://www.R-project.org/}.

\bibitem[{Radtkey and Singer(1995)}]{radtke1995}
Radtkey, R.~R., and M.~C. Singer.
\newblock 1995.
\newblock Repeated Reversals of Host-Preference Evolution in a Specialist
  Insect Herbivore Author.
\newblock Evolution {\bf 49}:351--359.

\bibitem[{Rauham�ki et~al.(2014)Rauham�ki, Wolfram, Jokitalk, Hanski, and
  Dahlhoff}]{plos14}
Rauham�ki, V., J.~Wolfram, E.~Jokitalk, I.~Hanski, and E.~P. Dahlhoff.
\newblock 2014.
\newblock Differenced in the Aerobic Capacity of Flight Muscles between
  Butterfly PPopulation and Species with Dissimilar Flight Abilities.
\newblock PLoS One .

\bibitem[{Rodrigues and Moreira(2004)}]{rodrigues04}
Rodrigues, D., and G.~R. Moreira.
\newblock 2004.
\newblock {Seasonal variation in larval host plants and consequences for and
  Heliconius erato (Lepidoptera and Nymphalidae) adult body size}.
\newblock Austral Ecology {\bf 29}:437--445.

\bibitem[{Rodr�guez et~al.(2007)Rodr�guez, Sullivan, Snyder, and
  Cocroft}]{rodriguez07}
Rodr�guez, R.~L., L.~M. Sullivan, R.~L. Snyder, and R.~B. Cocroft.
\newblock 2007.
\newblock Host Shifts and the Beginning of Signal Divergence.
\newblock Evolution {\bf 62}:12--20.

\bibitem[{Rutowski et~al.(1988)Rutowski, Gilchrist, and Terkanian}]{rutowski88}
Rutowski, R.~L., G.~W. Gilchrist, and B.~Terkanian.
\newblock 1988.
\newblock Male Mate-locating Behavior in Euphydryas chalcedona
  (Lepidoptera:Nymphalidae) Related to Pupation Site Preferences.
\newblock Journal of Insect Behavior {\bf 13}:277--289.

\bibitem[{Schneider et~al.(2012)Schneider, Rasband, and Eliceiri}]{imagej}
Schneider, C., W.~Rasband, and K.~Eliceiri.
\newblock 2012.
\newblock {NIH Image to ImageJ: 25 years of image analysis}.
\newblock Nature Methods {\bf 9}:671--675.

\bibitem[{Severns and Warren(2008)}]{severns08}
Severns, P., and A.~Warren.
\newblock 2008.
\newblock Selectively eliminating and conserving exotic plants to save an
  endangered butterfly from local extinction.
\newblock Animal Conservation {\bf 11}:476--483.

\bibitem[{Severns and Breed(2014)}]{severns14}
Severns, P.~M., and G.~A. Breed.
\newblock 2014.
\newblock Behavioral consequences of exotic host plant adoption and the
  differing roles of male harassment on female movement in two checkerspot
  butterflies.
\newblock Behavioral Ecolgoy and Sociobiology {\bf 68}:805--814.

\bibitem[{Singer(1983)}]{singer1983}
Singer, M.~C.
\newblock 1983.
\newblock Determinants of Multiple Host Use by a Phytophagous Insect
  Population.
\newblock Evolution {\bf 37}:389--403.

\bibitem[{Singer et~al.(1993)Singer, Thomas, and Parmesan}]{singer93}
Singer, M.~C., C.~D. Thomas, and C.~Parmesan.
\newblock 1993.
\newblock {Rapid human-induced evolution of insect-host associations}.
\newblock Nature {\bf 366}:681--683.

\bibitem[{Stamp(1979)}]{stamp1979}
Stamp, N.~E.
\newblock 1979.
\newblock New oviposition plant for Euphydryas phaeton (Nymphalidae).
\newblock Journal of the Lepidopterists' Society {\bf 33}:203--204.

\bibitem[{Stamp(1984)}]{stamp84}
Stamp, N.~E.
\newblock 1984.
\newblock {Effect of Defoliation by Checkerspot and Caterpillars (Euphydryas
  phaeton) and Sawfly Larvae (Macrophya nigra and Tenthredo grandis) on Their
  Host Plants (Chelone spp.)}.
\newblock Oecologia {\bf 2}:275--280.

\bibitem[{Takeuchi(2006)}]{takechi06}
Takeuchi, T.
\newblock 2006.
\newblock The effect of morphology and physiology on butterfly territoriality.
\newblock Behaviour {\bf 143}:393--403.

\bibitem[{Thompson(1998)}]{thompson1998}
Thompson, J.
\newblock 1998.
\newblock Rapid evolution as an ecological process.
\newblock Trends in Ecology and Evolution {\bf 13}:329--332.

\bibitem[{Velde and Dyck(2013)}]{velde13}
Velde, L.~V., and H.~V. Dyck.
\newblock 2013.
\newblock Lipid economy, flight activity and reproductive behaviour in the
  speckled wood butterfly: on the energetic cost of territory holding.
\newblock Oikos {\bf 122}:555--562.

\bibitem[{Wee(2004)}]{wee2004}
Wee, P.-S., 2004.
\newblock Effects of Geographic Distance, Landscape Features and Host
  Association on Genetic Differentiation of Checkerspot Butterflies.
\newblock Ph.D. thesis, The University of Texas at Austin.

\bibitem[{Wood and Guttman(1983)}]{wood83}
Wood, T., and S.~Guttman.
\newblock 1983.
\newblock Echenopa binotata Complex: Sympatric Speciation?
\newblock Science {\bf 220}:310--312.

\end{thebibliography}
