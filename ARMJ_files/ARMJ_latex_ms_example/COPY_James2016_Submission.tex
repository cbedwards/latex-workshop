\documentclass {article}
\usepackage[margin=1 in]{geometry}
\usepackage{xcolor}
\usepackage{outline}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{lineno}
\usepackage{natbib}
\usepackage{lscape}
\usepackage{amsmath}
\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\begin{document}

\doublespacing


\begin{titlepage}
\topmargin 1cm \centering \normalsize
\renewcommand{\baselinestretch}{1}


\Large Sex-specific morphological effects of a widely adopted exotic host plant on the native butterfly \textit{Euphydryas phaeton}.  

\vspace{1cm}

\normalsize Aubrie R. M. James*$^{1,2}$, Paul M. Severns$^{2, 3}$, Leone M. Brown$^{2, 4}$, Elizabeth E. Crone$^{2,4}$, and Greg A. Breed$^{2, 5}$

\renewcommand{\baselinestretch}{1}
\small \normalsize
\flushleft
$^{1}$Department of Ecology and Evolutionary Biology, Cornell University, Ithaca, New York 14853\\

$^{2}$Harvard Forest, Harvard University, Petersham, Massachusetts 01366\\

$^{3}$Department of Botany and Plant Pathology, Oregon State University, Corvallis, Oregon 97331\\

$^{4}$Department of Biology, Tufts University, Boston, Massachusetts 02153\\

$^{5}$Institute of Arctic Biology, University of Alaska, Fairbanks, Alaska, 99775\\

\vspace{3mm}
\centering *Corresponding author: aj465@cornell.edu\\

\vspace{1cm} 

\flushleft\textbf{Running head:} Effects of exotic host plant on butterfly morphology
\medskip


\blfootnote{\textbf{Author Contributions} ARMJ, EAC, and GAB conceived and designed the experiment. ARMJ and GAB performed the experiment in 2012, and LMB performed it in 2013. ARMJ and GAB wrote the manuscript; EAC, LMB, and PMS provided editorial advice.}

\end{titlepage}

\doublespacing
\linenumbers 
\section*{Abstract}
\noindent The specialist butterfly \emph{Euphydryas phaeton} (Nymphalidae) has recently incorporated the exotic host (\emph{Plantago lanceolata}) into its larval diet, but the consequences of this diet shift are not well studied in the adults of this species. Previous work has shown that larval \emph{E. phaeton} reared on \emph{P. lanceolata} had slower growth rates than those reared on its single historical host plant, \emph{Chelone glabra}. In 2012 and 2013, we tested if adult \emph{E. phaeton} that developed using the different host plants exhibit host-associated morphological differences, hypothesizing that adults that developed on \emph{P. lanceolata} would be smaller than those that developed on \emph{C. glabra}. To do so, we inoculated a set of wild-growing host plants with second and third-instar \emph{E. phaeton} larvae that had recently broken winter diapause. Upon their emergence from chrysalis, we measured adult wing size and body mass. Females were statistically unaffected by host plant, while males that developed on \emph{P. lanceolata} had significantly smaller wings and higher wing loadings than males that developed on \emph{C. glabra}.  This difference has the potential to affect males' access to females, as \emph{E. phaeton} males defend territories to secure matings. Smaller wings and higher wing loadings are expected to make male territorial defense and dispersal more energetically costly. Through this mechanism, males that develop as larvae using \emph{P. lanceolata} may be inferior competitors for females and have decreased potential to disperse. Consequently, the morphological effects of larval host plant on males may affect populations of \textit{E. phaeton}, and enforce selection for the  native host plant via sexual selection.   
\\
\\
\textbf{Keywords:} butterflies, morphology, novel hostplants, invasion biology, sexual dimorphism \medskip

\openup 1mm
\section*{Introduction}

Novel environmental conditions and interactions have quickly become common if not normal for many plant and animal species. In insect communities, interactions with invasive exotic plants are significant because herbivorous insects can quickly adopt invading species as host plants. Using new host plants may lead to subsequently changes in the behavior or morphology, and the resulting selection pressures can affect insects' evolutionary trajectories~\citep{wood83, singer93, dres02, rodriguez07}. As a mechanism of herbivorous insect evolution, host plant associated differentiation can cause insect lineages to diverge, generating new host races and perhaps leading to ecological species~\citep{Nosil:2007cj}. Host-associated differentiation appears to be manifested in similar ways across agricultural and natural systems, with either morphology, development, behavior, or a combination of these divergent traits reinforcing recently evolved host races, and there is a growing body of literature demonstrating the roles that morphological, behavioral, developmental, and mating preferences play in driving host-associated differentiation.  Many of these examples arise in agricultural systems, where crop pest evolution has been closely tracked and studied due to the impacts of economic loss \citep{pashley86, feder88, thompson1998, martel03}. Host-associated differentiation between native insects and exotic plants is not as thoroughly studied as interactions between exotic insects and native plants, but interactions between native insects and exotic plants are assuredly novel from an evolutionary perspective and may manifest host-associated differences in unanticipated ways. 

Butterflies in the genus \emph{Euphydryas} have long been used as a model system in ecology to understand population, metapopulation, and behavioral ecology \citep{harrison1989, munguira1997, boughton1998, ehrlich1992, laughlin2002, Joyce2003, ehrlich2004}.  \textit{Euphydryas}'s responses to larval host plants are of recent interest because many populations have adopted non-native host plants in disturbed or fragmented landscapes \citep{singer1983, Bowers1992, singer93, radtke1995, wee2004, severns14}. Studies of novel host use by \emph{Euphydryas spp.} have mostly focused on one life stage (adults), and cover dynamics at the scale of the population or metapopulation.  Individual changes in response to different larval host plants are relatively unexplored (but see Bowers et al., 1992 and Bowers and Stamp, 1992). Here, we more carefully examine the potential impact of host plant on individual butterflies in order to understand how individual-level effects might translate to population-level ecological and evolutionary impacts.  In particular, we tested if morphological differences in the adults of \emph{Euphydryas phaeton} (Nymphalidae), the only \emph{Euphydryas} found in eastern North America, were associated with development on the host plant to which they were historically specialized, \emph{Chelone glabra} (Plantaginaceae, L.) as compared to those that developed on a recently adopted exotic, \emph{Plantago lanceolata} (Plantaginaceae, L.).  

\emph{P. lanceolata} was likely introduced to North America in the 18th or early 19th Century, and is now naturalized and one of the most widely distributed plants on the continent \citep{cavers1980}. Several specialist native insects, including a number of species of \emph{Euphydryas}, have adopted this plant as a larval host, and numerous populations of \emph{E. phaeton} \citep{stamp1979, Bowers1992, severns14}, \emph{E. editha monoesis} \citep{singer93}, \emph{E. editha taylori} \citep{danby1890, severns08}, and \emph{Junonia coenia} \citep{bowers84} (all in Nymphalidae), have been shown to persist using only \emph{P. lanceolata} as a host \citep{severns14}.  In \emph{E. phaeton}, our study organism, use of \emph{P. lanceolata} as a primary host was first reported approximately 40 years ago \citep{stamp84, Bowers1992}.  Prior to adopting \emph{P. lanceolata}, \textit{E. phaeton} was specialized on a single larval host, \textit{C. glabra}, over almost all of its range \citep{stamp84, Bowers1992}. 

One way to understand how use of the new exotic host affects the reproductive potential and fitness of adults is examine if and how physical differences exist between between adults that developed from their larval stages on the two different host plants. Larval diet in known to play a major role in determining adult phenotype in insects~\citep{Boggs05, pellengroms09, Gibbs12}.  Moreover, there is evidence that these two host plants are not equivalent: Bowers found that pre- and post-diapause larvae (larvae before and after overwintering) reared on \emph{C. glabra} were larger and increased in mass more quickly than those on \emph{P. lanceolata}, \citep{bowerschem}. In other butterflies, nutritional stress of larvae slows larval development, produces smaller adult butterflies, and alters wing shape and wing loading \citep{Boggs05, pellengroms09, Gibbs12}. Studies of alternative host plants in other insects have yielded similar results; use of different larval host plants is associated with is size differences in adults \citep{rodrigues04, Mozaffarian07}.

Here, we tested how the historical host \emph{C. glabra} and novel exotic host \emph{P. lanceolata} affect wing size, shape, and overall mass of adult \emph{E. phaeton}.  We hypothesized that individuals that developed on \emph{P. lanceolata} would be smaller, consistent with findings of larval growth rates reported by \citep{bowerschem}.  If differences are present, changes in morphology would be expected to affect courtship, mating success, female fecundity, and dispersal, all of which are integral to butterfly life history and fitness \citep{kingsolver99, paegeria02, Kemp10, Almbro12}. To perform this test, we measured morphological traits critical to flight in adult \emph{E. phaeton} that developed on either \emph{C. glabra} or \emph {P. lanceolata} in a natural community containing all three species. 

\section*{Materials and Methods}
\paragraph{Life history background.}\textit{E. phaeton} is obligately univoltine. Eggs are laid in large clusters in midsummer and hatch 3 weeks later. Larvae feed for several weeks before entering diapause and overwinter in loose silken hibernacula in their third instar. Diapause is broken the following spring, pupation occurs from late May to late June, and adults eclose (emerge from chrysalis) approximately 10 days later.  Diet of pre-diapause larvae is highly specialized; most \textit{E. phaeton} populations use \emph{C. glabra} exclusively during this time. Post-diapause larvae have been reported to use a somewhat wider range of iridoid-containing hosts, but larvae still generally use either \textit{C. glabra} or \textit{P. lanceolata} \citep{bowers80}.  As stated, \textit{E. phaeton} has been documented to use \emph{P. lanceolata} as a viable host for both oviposition and prediapause larvae within the last 30 years \citep{Bowersrange}, and some populations now appear exclusively supported by this host plant \citep{severns14}. 

After eclosure, adults are short-lived, flying 4-10 days.  During this time, males set up and defend stable territories. Territories are defended aggressively.  Challenging males may be chased away; if they are not, they will engage the territory holder in sweeping, helical aerial demonstrations that are largely won out by endurance (\emph{sensu} Kemp, 2010).  Once mated, females will alternate between nectaring and searching for oviposition sites. \emph{P. lanceolata} and \emph{C. glabra} are the only plants females have been observed to oviposit on, where gravid females prefer \emph{C. glabra} over \textit{P. lanceolata} for oviposition (Breed et al., unpublished data). 

\subsection*{Field Design} 
Our study focused on a small \textit{E. phaeton} population located on a $\sim$ 2 hectare mosaic of wet meadows, active hay field, and forested hedgerows in Harvard Township, Massachusetts ($42.50^{\circ}$ N, $71.57^{\circ}$ W). This site has naturally occurring populations of \emph{C. glabra}, \emph{P. lanceolata}, and \emph {E. phaeton}.

To measure field survival of post-diapause larvae feeding on different host plants, we placed closed-top, hemispherical enclosures in different locations in the site.  Enclosures were open at the bottom so they could be staked over naturally growing vegetation. We placed twelve enclosures over patches of \textit{P. lanceolata}, and eleven over \textit{C. glabra} patches. In early May 2012, just after larvae broke winter diapause, we collected third-instar larvae and used them to inoculate each enclosure. Each enclosure was inoculated with four larvae. This density was expected to be low enough that the host plant resource within the enclosures was not exhausted and thus not considered as a limiting factor in the survival or growth of the larvae.  After inoculation, we checked enclosures regularly (every 1-3 days) to ensure the host plant was not depleted. The experiment was repeated over two years, 2012 and 2013. Weight data were collected in both years, while wing morphology measurements were collected only in 2012.

\subsection*{Morphological Measurements}
After larvae began pupating, we checked enclosures daily for newly emerged butterflies. Upon emergence, butterflies were removed from their enclosures, measured, marked, and released as part of a larger mark-recapture study. We measured two key morphological traits that affect butterfly flight ability: the ratio of thorax weight to wing size, and thorax weight \citep{ehrlich2004}. Because individuals in this study were part of a larger mark-recapture study and could not be killed, it was not possible to gather dry weights of thoraxes. Instead, we measured the wet weight and wing area in the field without harming butterflies. To measure weight, we placed each butterfly into a 250-ml plastic cup which was then closed in an ice chest for 15 minutes. Chilling them allowed us to weigh each butterfly on a microbalance. Weight measurements were taken in 2013 using the same protocol and on the same population of butterflies. 

After weighing, we photographed each animal's left, ventral wings three times against a 1$\times$1 mm gridded card. The card, a 7.5 x 10 cm piece of laminated paper, was placed perpendicular to the thorax, between the wings, and flush with the left forewing. The photo with the highest clarity, least angular distortion, and best wing position on the gridded card was used for digital measurement of the area of each butterfly's left wing. Wing areas were calculated using the image analysis program ImageJ ~\citep{imagej}. 

Obvious morphological differences between the sexes were apparent because the species is sexually dimorphic. Consequently, we analyzed the effects of larval host on male and female adults separately. Morphological data were analyzed using the \texttt{lme4} package in the program R \citep{lmer, R}, using a linear mixed model with host plant treatment as the fixed effect and replicate enclosure as the random effect. Response variables were one of three measures or derived measures: wing area, weight, and wing loading (weight per unit wing area).

\section*{Results}
In 2012, 39 butterflies from the enclosures survived to adulthood, 34 of which were used in our morphological analyses. The remaining five individuals did not have a full complement of measurements for morphological analyses (for example, some had a defect which caused their wings to be grossly asymmetrical; such defects occurred on both hosts), and were omitted. In 2013, 98 butterflies (33 females and 65 males) survived to adulthood, all of which were weighed. No other metrics were recorded. Because of this, we use only the butterflies from 2012 for morphological analyses. We measured the weight, wing area, and body length of 9 females and 9 males from \emph{C. glabra} enclosures and 9 females and 7 males from \emph{P. lanceolata} enclosures. All morphological results are reported with a 95\% confidence interval. 

\subsection*{Female Results}
In 2012, female weight did not significantly differ between hosts; females reared on \emph{C. glabra} averaged 0.291 $\pm$ 0.051 grams, and females reared on \emph{P. lanceolata} averaged 0.304 $\pm$ 0.049 grams (Figure~\ref{fig:boxplots}). Females also did not differ between treatments in their wing areas or wing loadings; females reared on \emph{C. glabra} had wings with average area 467 $\pm$65 mm$^2$ , whereas \emph{P. lanceolata} females had an average wing area of 478 $\pm$ 63mm$^2$. Wing loadings were 6.5 $\times$ 10$^{-4}$ g$\times$mm$^{-2}$ $\pm$ 1.1 $\times$ 10$^{-4}$ in the \emph{C. glabra} treatment, and 6.4 $\times$ 10$^{-4}$ g$\times$mm$^{-2}$ $\pm$ 1.1 $\times$ 10$^{-4}$ in the \emph{P. lanceolata} treatment (Table~\ref{table:measurements}). 

Similarly, in 2013, female weight was not statistically significantly different between host plant treatments: females reared on \emph{C. glabra} had an average weight of 0.338 $\pm$ 0.018 grams, and females reared on \emph{P. lanceolata} averaged 0.310 $\pm$ 0.025 grams.  (Figure~\ref{fig:wts4}).

\subsection*{Male Results}
Nine males reared on \emph{C. glabra} and seven reared on \emph{P. lanceolata} survived to adulthood. Males, due to the sexual dimorphism in this species, were smaller than females in both wing area and weight. As with females, male weights did not differ between host plant treatments: on \emph{C. glabra}, they averaged 0.187 $\pm$ 0.059 grams, whereas on \emph{P. lanceolata} they averaged weights of 0.226 $\pm$ 0.059 grams (Figure~\ref{fig:boxplots}). Difference in wing areas between the two treatments, however, was large and significant: males reared on \emph{C. glabra} had average wing areas of 416 $\pm$ 23 mm$^{-2}$, and males reared on \emph{P. lanceolata} 303 $\pm$ 25mm$^{-2}$ (Figure~\ref{fig:boxplots}).  Because weights were statistically equivalent between the two groups and wing areas were much smaller in the \emph{P. lanceolata} males, wing loading should also differ between the treatments, and in fact they did: \emph{P. lanceolata} males had wing loadings of 8.7 $\times$ 10$^{-4}$ g$\times$mm$^{-2}$ $\pm$ 2.6 $\times$ 10$^{-4}$, while \emph{C. glabra} males had wing loadings of about half that: 4.5 $\times$ 10$^{-4}$ g$\times$mm$^{-2}$ $\pm$ 2.4 $\times$ 10$^{-4}$.

Differences in male weights remained statistically insignificant between host plant treatments in 2013. Males reared on \emph{C. glabra} averaged 0.197 $\pm$ 0.014 grams, while \emph{Plantago lanceolata} males averaged 0.180 $\pm$ 0.014 grams (Figure~\ref{fig:wts4}).

\section*{Discussion} 
\par\noindent Few studies report on the morphological differences that arise as a consequence of adopting novel host plants~\citep{dres02}, outside of the traits that directional selection should favor on the recently adopted host plant, such as ovipositor length \citep{DORCHIN:2009cb}, beak size \citep{Carroll:1997fx}, and cryptic coloration \citep{Nosil:2007cj}. At the outset of this study, we expected host plants would effect adult morphology, consistent with findings of larval growth in this species \citep{Bowers1992} and adults in other insects \citep{rodrigues04, Mozaffarian07, Boggs05, pellengroms09, Gibbs12}.  Specifically, we hypothesized that metrics we measured on adults reared on the exotic host would be smaller.  From our findings, we conclude that our hypothesized effect does occur, but the mechanism is sex-specific and the more complex than expected. Females appear unaffected, while males are the same size but have smaller wings. The consequences of this result are similar but somewhat less straightforward than the hypothesized effect of simply being smaller. In particular, the results imply minimal effect on female fecundity or fitness, but increased male wing loading is expected to affect their territory defense, mate acquisition, and dispersal. Although the morphological consequences of host-associated differences following the acquisition of exotic species are mostly unknown, our study suggests that unanticipated changes could develop when insects adopt exotic host plant species.  

\par In \textit{E. phaeton}, like other checkerspot butterflies, males generally emerge from pupae before females, and subsequently perch on vegetation near areas they expect virgin females to emerge or pass by. When a female is visually sighted, a male will begin courtship. In this mating system, males defend prime perching sites vigorously because those who are able to defend and maintain key locations obtain more matings~\citep{rutowski88, Kemp10, Allen11}. Males rely in part on energetic efficiency for territory defense. Studies have demonstrated that lower wing loadings (lower ratios of unit weight per unit wing area) are tied to increased flight efficiency in males for some butterfly species~\citep{angelo84, imafuku06, takechi06, pellengroms09}. Lower wing loading may aid males in securing and defending territory by decreasing the energetic cost of the so-called wars of attrition between competitors \citep{hammerstein82, kemp01, takechi06}. As such, wing size likely has direct consequence to the amount of matings a male \textit{E. phaeton} will have over the course of his brief adult life. Because \textit{E. phaeton} males develop smaller wings when they develop using \textit{P. lanceolata} compared to their historical specialist host, they may be an inferior sexual competitors.  Thus \textit{P. lanceolata} may be an inferior host for males, but the effect would be mediated by sexual selection.

\par Wing loading may also affect dispersal in \textit{E. phaeton}. Previous work has shown that some male butterflies are likely to disperse from environments with low local male density to high local male density because males are more likely to find a mate if they are among other males~\citep{bradbury1983, beehler88}. A butterfly's ability to disperse is related to its wing loading~\citep{imafuku06, takechi06, velde13}, and higher wing loadings make dispersal energetically costly.  Butterflies like the males that developed on \emph{P. lanceolata} may have a lower probability of successful dispersal from their natal environment~\citep{dudley02, imafuku06, takechi06, velde13,  plos14}. 

\par Physiological changes at the individual level may affect higher-level ecological and evolutionary processes such as mating behavior and dispersal (e.g. \citealt{Emelianov:2001cu, Nosil:2007cj, DORCHIN:2009cb}), and our data show that host plants used by larval have the potential to affect adult morphology. In other lepidoptera, mate choice, oviposition behavior, and mate selection change in response to larval host plant species~\citep{knuttel2001, goverde2008, forister2012}.  \textit{Euphydryas phaeton}'s novel, exotic host, \emph{P. lanceolata} causes males to have smaller wings and higher wing loadings as compared to those the develop on their historical specialized \textit{C. glabra}.  The trajectory of \emph{Euphydryas phaeton} will depend on its population-level response to increasing amounts of \emph{P. lanceolata} on the North American landscape and their ability to evolve. To test for this, subsequent research in this system should focus on quantifying the consistency of this pattern through time and in other populations of \textit{E. phaeton}, the presence of this phenomenon in other species in Nymphalidae which have adopted \textit{P. lancelota} as a host plant, and a thorough investigation of the physiological mechanisms of wing development in males and females that might be affected by metabolites in the different host plants.

\paragraph{Acknowledgments}
The authors would like to thank M. Geber, R. Petipas, and C. Mittan for helpful comments on previous versions of the manuscript, C. Kilgore and B. Ziemba for help in the field, and the alumni of the Harvard Forest 2012 R.E.U. program. A.R.M. James was supported on an R.E.U. fellowship awarded through the National Science Foundation. This work was partly supported by a Department of Defense SERDP awarded to E. Crone (RC-2119).

%\paragraph{Data Accessibility}
%All data used in this manuscript are present in the manuscript and its supporting information.

\clearpage
\bibliography{COPY_James2016_Submission}
\bibliographystyle{ecology}

\clearpage
\begin{table}
\centering
\caption{Morphological Measurements} \vspace{1mm}
\begin{tabular}{l l l l l}
\hline
 & \multicolumn{2}{c}{\textbf{Female}} & \multicolumn{2}{c}{\textbf{Male}} \\ 
\textbf{Measurement} & \emph{P. lanceolata} & \emph{C. glabra} & \emph{P. lanceolata} &\emph{C. glabra} \\ \hline
Mean Weight (g) &&&&\\
\hspace{3mm} 2012&0.304&0.291&0.226&0.187 \\ 
\hspace{3mm} 2013&0.310&0.337&0.180&0.197 \\
Weight Variance &&&&\\
\hspace{3mm} 2012&0.67&0.51&1.36&0.16\\
\hspace{3mm} 2013&0.002&0.001&0.001&0.001\\
Mean Wing Area ($mm^2$)&478&467&303&416 \\ 
Wing Area Variance&1.3&0.63&0.15&0.16\\
Mean Wing Loading (g$\times$$mm^{-2}$) & 6.5 $\times$ $10^{-4}$ & 6.4 $\times$ $10^{-4}$  & 8.7$\times$ $10^{-4}$&4.5$\times$$10^{-4}$\\ 
\hline
\end{tabular}
\label{table:measurements}
\end{table}

\clearpage

\begin{figure}
\includegraphics[width=1\textwidth]{2016boxplots.pdf}
\caption{Boxplots, means, and confidence intervals of female and male measurements grouped by host plant treatment. Boxplots with small open circles show the median and spread of the morphological data. Open or closed triangles and circles next to the boxplots depict the mean and 95\% confidence interval of the same data. Females do not differ in either wing area or weight. Males have significantly smaller wings when reared on \emph{P. lanceolata}.}
\label{fig:boxplots}
\end{figure}

\begin{figure}
\includegraphics[width=1\textwidth]{wts4.pdf}
\caption{Butterfly weights in 2012 and 2013, grouped by sex and larval host plant. Weight differences between years were not statistically significant for males or females.}
\label{fig:wts4}
\end{figure}


\end{document}

