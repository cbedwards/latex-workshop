\documentclass[12pt]{article}
\usepackage{times}
\usepackage[margin=1in]{geometry}
\usepackage[table]{xcolor}
\usepackage{hyperref}
\usepackage{array}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{paralist}
\usepackage{array}
\usepackage{pdflscape}
\usepackage{longtable}

\newcolumntype{$}{>{\global\let\currentrowstyle\relax}}
\newcolumntype{^}{>{\currentrowstyle}}
\newcommand{\rowstyle}[1]{\gdef\currentrowstyle{#1}%
  #1\ignorespaces
}
\newcolumntype{P}[1]{>{\raggedright\arraybackslash}p{#1}}

\usepackage{fancyhdr}

\hypersetup{colorlinks,urlcolor=blue}

\usepackage{fancyhdr}
\lhead{\textcolor{darkgray}{BioEE1640 Course Syllabus v3.0 (updated \today )}}
\rhead{\textcolor{darkgray}{Fall 2013}}
\renewcommand{\headrulewidth}{0pt}

\begin{document}
\pagestyle{fancyplain}
\begin{center}
\large{\textbf{BioEE1640: On the Origin and Future of Biodiversity}}
\end{center}
\begin{center}
\line(1,0){250}
\end{center}

\noindent\textbf{Instructor Information:} \\
Instructor: Nicholas A. Mason (I usually just go by Nick)\\
E-mail: \href{mailto:nam232@cornell.edu}{nam232@cornell.edu} \\
Office: Corson Hall E231\\
Office Hours: Tuesday 9:00 -- 10:00 a.m., or by appointment \\

\noindent \textbf{Course Information:} \\
Meeting Times: Tuesday \& Thursday, 10:10 -- 11:25 a.m. \\
Room: Plant Science Building 141 \\

\noindent\textbf{Official Course Description:}

\noindent Over the last three and a half billion years, our planet has witnessed the evolution of millions of species. From
the giant Blue Whale to the diminutive House Mouse, each species tells a unique evolutionary tale; these stories
are shaped by the shifting of the Earth�s continents, dramatic glaciation events, and the myriad of organisms that
share their habitat. This course will simultaneously develop students� writing skills and a greater understanding of
Earth�s biodiversity through critical readings of both scientific literature and popular media, including scientific
blogs such as Carl Zimmer's \textit{The Loom}. Writing exercises will first guide students in effective communication of
scientific data, results, and hypotheses. As the semester progresses, students will be allowed to pursue their
personal interests in plant and animal diversity by writing and peer-reviewing essays focused on the patterns and
processes underlying Earth's biodiversity.

\bigskip

\noindent\textbf{Learning Outcomes:}

\setdefaultleftmargin{1em}{1em}{}{}{}{} 
\begin{compactitem}
\item \textbf{You will improve your writing ability for academic coursework and professional writing.} \\
This course will require you to read, critique and construct various forms of writing. The goal of this is to enhance your ability to write effectively, which is an extremely important skill in any field or profession.
\item \textbf{You will develop effective research and communication skills.}\\
Various assignments will require you to find, interpret and properly cite both primary and secondary sources of information. In addition to writing, this class will also focus on improving your ability to communicate through discussions and oral presentations.
\item \textbf{You will acquire a basic knowledge of evolutionary biology and conservation.}\\
Through writing, reading and discussing various works regarding the origin and future of biodiversity, you will gain a basic understanding of the ecology, evolutionary biology and conservation concerns of various species. 
\end{compactitem}
\bigskip

\noindent\textbf{Optional Texts:}

\noindent I will provide relevant chapters from these texts to students via Blackboard. However, if you find the readings interesting and would like to learn more, I highly recommend these texts in their entirety.

\bigskip

\begin{compactitem}
\item Quammen, D. 1996. \textit{Song of the Dodo}. Schribner Press. Available \href{http://www.amazon.com/The-Song-Dodo-Biogeography-Extinction/dp/0684827123}{here} from Amazon.
\item Zimmer, C. 2009. \textit{The Tangled Bank}. Roberts and Company Publishers. Available \href{http://www.amazon.com/Tangled-Bank-Introduction-Evolution/dp/0981519474/ref=sr_1_1?s=books&ie=UTF8&qid=1377607126&sr=1-1&keywords=the+tangled+bank}{here} from Amazon.
\item Coyne, J. 2010. \textit{Why Evolution is True}. Penguin Books. Available \href{http://www.amazon.com/Why-Evolution-True-Jerry-Coyne/dp/0143116649/ref=sr_1_1?s=books&ie=UTF8&qid=1377607224&sr=1-1&keywords=Why+Evolution+is+true}{here} from Amazon.
\end{compactitem}

\bigskip

\noindent\textbf{Description of required writing:}

\noindent Writing assignments for this seminar will consist of six essays that will be accompanied by short reading responses and in-class writing exercises. Four of these essays will undergo peer review and/or receive feedback from me prior to submission of a 'final' draft. Essays will include a mixture of research-based assignments and narratives to engage in a wide variety of writing styles and formats, but the main emphasis will be on academic writing in the life sciences.
 
 \bigskip

\noindent \textbf{Assignment Guidelines:}
\begin{compactitem}
\item Word-process all written work (either MS Word or Pages)
\item 12 pt Times New Roman font
\item Double-spaced
\item 1 inch margins on all sides
\item Numbered pages
\item Your name, assignment name/number, and date and essay title at the top of the first page
\item Proofread and spellcheck each assignment
\item Final drafts will be submitted via turnitin

\end{compactitem}

\bigskip

\noindent \textbf{Individual Conferences:}\\
Each student will be required to meet with me at least \textbf{twice} over the course of the semester. I am, of course, willing to meet with anyone at any time to discuss their writing or performance in the class. One of these meetings will occur close to midterms. At that point, we will discuss your performance in the class so far, where your grade stands and how to improve your standing by the end of the semester. The second required meeting will be an exit exam during finals week. You are more than welcome to schedule additional meetings to discuss your writing, performance in the class or anything else you might like to chat about!

\bigskip

\noindent \textbf{Grading Policy:}\\
\textit{Contract Grading}\\
The primary objective of this course is to improve your skill as a writer. Traditional grading schemes  often hinder this goal by shifting focus from course content to concerns about individual assessment. Therefore, I have chosen to employ a grading system that gives you more power over your grade and deemphasizes the grade that an individual assignment may or may not deserve in favor of promoting the continuous improvement and development of your writing ability.

\bigskip

\noindent Here's how it works. \textbf{You are guaranteed a B in this course if you:}

\begin{compactenum}
\item Attend every class.
\item Show up to every class \textit{on time}.
\item Turn in every assignment on time.
\item Do all of the readings.
\item Actively participate in class discussions.
\item Actively participate in peer reviews.
\item Strive to improve your writing with every new draft or assignment.
\end{compactenum}
\bigskip
\noindent\textbf{You will receive either a $\checkmark+$, $\checkmark$ or $\checkmark-$ in each of these categories after every class and for every assignment.} I will record this information on Blackboard and will do my best to update it after every class. Only you will have access to your assessments and it will be available to you at all times. That way, you will always know where you stand, and you can check it as often as you like.

\bigskip

\noindent The way to receive a check for \#1--3 should be straightforward. For example, if you come to class, you receive a check. If you don�t, you will receive a minus. My assessments of \#4--7 will depend upon your participation in class activities, such as group discussions or in-class responses to reading assignments.\\

\noindent\textit{Free Passes}\\
I recognize that we are all human, and that life happens. So, you are allowed two `free passes' for \textit{each} of the rules above. This means that you will still receive a B, even if you:
\begin{compactenum}
\item Miss up to two classes.
\item Are up to 10 minutes late for up to two classes.
\item Turn in two assignments one day late.
\item Receive a $\checkmark-$ for any two reading assignments.
\item Receive a $\checkmark-$ for any two class discussions.
\item Receive a $\checkmark-$ for any two peer reviews.
\item Receive a $\checkmark-$ for any two writing assignments, including drafts.
\end{compactenum}

\bigskip

\noindent Please note that if you miss a day of class, you will be responsible for finding our from other students (not me) what happened in your absence -- you will need to get copies of materials, assignments, and figure out what was covered in class through your peers. Barring any extreme circumstances, I will NOT make exceptions beyond those listed above. Failure to meet these requirements will the loss of a partial grade for each infringement (B will become a B- if you have more than two unexcused absences). \\

\noindent\textit{Grades Above a B}\\
Satisfying the criteria above will guarantee you a B in the course. However, grades above and below a B will be based on your level of engagement and effort in the class, as well as the quality and improvement of your writing. Simply put, if you satisfy all the criteria above and do excellent work you will receive an A. You will earn a grade higher than a B if, consistently throughout the semester, you thoroughly think about the readings and provide particularly insightful talking points, your writing vastly improves in ways we have discussed, you produce stellar papers, your drafts evolve in an impressive manner, you provide in-depth edits for your peers, etc...

\bigskip

\noindent\textbf{The public domain:}\\
Because the primary goal of this course is to improve everyone's writing ability, all student writing for the course may be read and shared by all members of the class. When discussing written excerpts with the entire class, I will strive to keep works anonymous. 

\bigskip

\noindent \textbf{Electronics Policy:}\\
Unless I give direct permission, electronics are not to be used during class. This includes, but is not limited to: laptops, cell phones, mp3 players, and tablets. Having electronics out, whether in active use or not, is distracting for everyone. \textbf{Two infractions (whether in the same class period or not) will result in one $\checkmark -$ for attendance.}
\bigskip

\noindent \textbf{Statement on University Policies and Regulations:}\\
I respect and uphold University policies and regulations pertaining to the observation of religious holidays; assistance available to the physically handicapped, visually and/or hearing impaired student; plagiarism; sexual harassment; and racial or ethnic discrimination. All students are advised to become familiar with the respective University regulations and are encouraged to bring any questions or concerns to my attention.\\
\\
\noindent \textbf{Statement on Students with Disabilities:}\\
In compliance with the Cornell University policy and equal access laws, I am available to discuss appropriate academic accommodations that may be required for students with disabilities. Requests for academic accommodations are to be made during the first three weeks of the semester, except in unusual circumstances, so that arrangements can be made. Students are encouraged to register with Student Disability Services to verify their eligibility for appropriate accommodations.\\
\\
\noindent \textbf{Statement on Academic Integrity:}\\
All the work you submit in this course must have been written for this course and not another and must originate with you in form and content with all contributory sources fully and specifically acknowledged. Make yourself familiar with Cornell�s Academic Integrity Code, which is available at www.theuniversityfaculty.cornell.edu/AcadInteg/. In this course, the normal penalty for a violation of the code is an F for the term.\\
\\
\noindent \textbf{Statement on TurnItIn:}\\
Students agree that by taking this course all required papers may be subject to submission for textual similarity review to Turnitin.com for the detection of plagiarism. All submitted papers will be included as source documents in the Turnitin.com reference database solely for the purpose of detecting plagiarism of such papers. Use of the Turnitin.com service is subject to the \href{http://turnitin.com/en_us/about-us/privacy-center/usage-policy}{Usage Policy posted on the Turnitin.com site}.

\bigskip

\noindent \textbf{Calendar:} \\
\textbf{Please note that the specific dates and assignments listed here may be subject to change depending on the trajectory of the course.} In the event of a change in the syllabus, students will be notified and an updated version will be distributed.

\newpage
\rowcolors{2}{gray!25}{white}
\begin{landscape}
\noindent \textbf{NOTE THAT ALL ASSIGNMENT AND DUE DATES ARE SUBJECT TO CHANGE. YOU WILL BE NOTIFIED OF ANY ADJUSTMENTS MADE TO THE SCHEDULE BELOW.}
\begin{longtable}{ccP{4cm}P{4.2cm}P{4.2cm}P{3.8cm}}
\hline
 \rowcolor{gray!50}
 \hline
  \hline
\textbf{Week} & \textbf{Date} & \textbf{Topic} & \textbf{Assigned Reading} & \textbf{Assigned Writing} & \textbf{What's due?} \\
\hline
 \hline
1 & 8/29/13 & Introduction to Course & Wake (2012); \newline Zimmer (2013) & Essay \#1 & -- \\
2 & 9/3/13 & Research Tutorial pt. 1 @ Library  & Stuart et al. (2004)  & -- & -- \\
 & 9/5/13 &Transition to College Writing; \newline Amphibian Crisis & Meyers et al. (2000)& Essay \#2 (proposal)  & Essay \#1\\ %Coyne (2009) Ch. 1 \& 2; \newline 
3 & 9/10/13 & Origins of Biodiversity & Coyne (2009) Ch. 4 & Essay \#2 (1$^{st}$ draft)  & Essay \#2 (proposal)  \\
 & 9/12/13 & Origins of Biodiversity & -- & -- & -- \\
4 & 9/17/13 & Origins of Biodiversity & Zimmer (2009) Ch. 10 & Essay \#2 (final)  & Essay \#2  (1$^{st}$ draft) \\
 & 9/19/13 & Origins of Biodiversity & -- & -- & -- \\
 5 & 9/24/13 & Origins of Biodiversity & Barnosky et al. (2012) & Essay \#3 (proposal) & Essay \#2 (final)  \\
 & 9/26/13 & Endangered Species & -- &  -- & -- \\
 6 & 10/1/13 & Endangered Species & Fitzpatrick et al. (2005); Gotelli et al. (2011) & Essay \#3 (1$^{st}$ draft) & Essay \#3 (proposal)   \\
 & 10/3/13 & Research Tutorial pt. 2 @ Library & -- & -- & -- \\
7 & 10/8/13 & Endangered Species & -- & -- & Essay \#3 (1$^{st}$ draft) \\
 & 10/10/13 & Visit to the Lab of Ornithology & -- & -- & -- \\
8 & 10/15/13 & \textbf{Fall break} & -- & -- & --\\
 & 10/17/13 & Endangered Species & Elliott et al. (2001) & Essay \#3 (final draft)  & -- \\
9 & 10/22/13 & Endangered Species & Wild Dog Reading & Essay \#4 (1$^{st}$ draft ) & Essay \#3 (final draft)  \\
 & 10/24/13 & Endangered Species & -- & -- &  --  \\
10 & 10/29/13 & Endangered Species & Tiger Reading & Essay \#4 (final draft) & Essay \#4 (1$^{st}$ draft) \\
 & 10/31/13 & Endangered Species & --  & --  & -- \\
11 & 11/5/13 & Narratives in Nature & Wilson (2010); \newline Kingsolver and Hopp (2001) & Essay \#5 (1$^{st}$ draft) & Essay \#4 (final draft) \\
 & 11/7/13 & Narratives in Nature & -- & -- & -- \\
12 & 11/12/13 & Narratives in Nature & Stanford (2000); \newline Kerasote (2001) & Essay \#5 (final draft) & Essay \#5 (1$^{st}$ draft) \\
 & 11/14/13 & Narratives in Nature & -- & -- & -- \\
13 & 11/19/13 & Conflicts in Conservation & Donlan et al. (2006) & Essay \#6 (1$^{st}$ draft) & Essay \#5 (final draft)\\
 & 11/21/13 & Conflicts in Conservation & -- & -- & -- \\
14 & 11/26/13 & Conflicts in Conservation &  -- & Essay \#6 (final draft) & Essay \#6 (1$^{st}$ draft) \\
 & 11/28/13 & \textbf{Thanksgiving break} & -- & -- & -- \\
15 & 12/3/13 & Conflicts in Conservation & Cahill et al. (2012) & -- & -- \\
 & 12/5/13 & Conflicts in Conservation & -- & -- & Essay \#6 (final draft) \\
 Exams & 12/8/13 & Exit Interviews & -- & -- & \textbf{All Exit Interviews Completed by 5 pm} \\
\hline

\end{longtable}
\end{landscape}
\end{document}