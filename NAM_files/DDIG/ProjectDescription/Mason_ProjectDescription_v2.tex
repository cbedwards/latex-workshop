%% Must use XeLaTeX compiler in order to use Arial font

\documentclass[11pt]{article}
\usepackage{fontspec}
\setmainfont{Arial}
\usepackage[margin=1in]{geometry}
\usepackage{tabu}
%\usepackage{showframe}
\usepackage{enumitem}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage[labelfont=bf,font={small}]{caption}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{array}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

%%%BibLaTeX

\usepackage[style=authoryear-comp,backend=bibtex,sorting=nyt, maxbibnames=2,maxcitenames=2,firstinits=true]{biblatex}%max authors?
\usepackage{xpatch}

\setlength\bibhang{0.5in}
\bibliography{2015DDIG}

\xpatchbibmacro{date+extrayear}{%
  \printtext[parens]%
}{%
  \setunit{\addperiod\space}%
  \printtext%
}{}{}
\renewbibmacro{in:}{}

%\nocite{*}

\renewbibmacro*{volume+number+eid}{%
  \printfield{volume}%
  \setunit*{\addcolon}
   \printfield{number}}
   
\DefineBibliographyStrings{english}{%Remove 'References' header
  references = {},
}

\DefineBibliographyStrings{french}{%
  pages = {},
}

\DefineBibliographyStrings{english}{%
  pages = {},
}
\DeclareFieldFormat[article]{title}{#1}
   
\begin{document}

%\pagenumbering{gobble}

\begin{center}
\textbf{Project Description}
\end{center}

\vspace{-6pt}

%\raggedright

\noindent\textbf{Introduction}

Phenotypic variation is ubiquitous among species, populations, and individuals. However, phenotypic and taxonomic diversity are unevenly distributed across space, time, and branches of the tree of life, which makes inferring the evolutionary processes that generate and maintain biodiversity an enduring goal of systematic biology. Lineages also vary in their ability to adapt to temporally and spatially variable selective pressures \autocite{West-Eberhard1989,Agrawal2001}. Variation among lineages' capacity for evolution across an adaptive landscape influences geographic distributions, the prevalence of local adaptation, and patterns of diversification among populations and species \autocite{Price2003,Arnold2001}. Studying geographic variation in adaptive traits among populations sheds light on the evolutionary processes that shape recent genetic and phenotypic differentiation, while phylogenetic comparative analyses can reveal the evolutionary processes underlying lineage and trait diversification among species. Combining population-level and species-level analyses of phenotypic diversity offers a synergistic perspective regarding the evolutionary phenomena that shape biodiversity across different taxonomic and temporal scales. \textbf{Here, I propose to study patterns of phenotypic and genetic differentiation within a recent, widespread, and highly polytypic genus of songbirds combined with comparative analyses of trait evolution at the family level to integratively study intraspecific and interspecific evolutionary dynamics of local adaptation in coloration, speciation, and lineage diversification in larks (Alaudidae).}

Coloration is a prominent phenotype that experiences a wide array of selective pressures, including both sexual selection (e.g., communication and species recognition; \textcite{Andersson2006}) and natural selection (e.g., aposematic or cryptic coloration; \textcite{Endler1978}). Studies of cryptic coloration, or camouflage, have provided seminal examples of natural selection and rapid evolution \autocite{Kettlewell1955, Hoekstra2006,Rosenblum2010}. Although camouflage is a widespread phenomenon among both invertebrate and vertebrate taxa, most studies of camouflage have been conducted on a small number of systems with discrete morphs over small spatial scales, rather than continuous, more complex patterns of color variation over broad geographic regions \autocite{Stevens2009}. The fitness consequences and genetic architecture of variation in cryptic coloration is well-studied in certain systems \autocite{Linnen2009,Linnen2013,Rosenblum2010,Hof2011}; however, the prevalence, adaptive significance, and genetic architecture of adaptive dorsal coloration via background matching remains unstudied in many taxa. 

Taxa vary in their ability to adapt to varying environmental conditions, which affects population-level processes of colonization and persistence \autocite{Baldwin1986}, thereby influencing species' geographic distributions and range limits \autocite{Sexton2009,Kirkpatrick1997}. Furthermore, the ability to colonize new areas and evolve adaptive phenotypes promotes genetic differentiation through divergent selection and geographic isolation from the ancestral population \autocite{Price2003}. \textbf{Thus, the evolutionary processes underlying local adaptation and geographic variation among populations can also impact rates of speciation and diversification among lineages, thereby providing a link between microevolutionary and macroevolutionary processes.} However, empirical studies that link intraspecific patterns of phenotypic and genetic variation and interspecific patterns of diversification are scarce. \textbf{In this DDIG, I propose to analyze associations between local variation in adaptive coloration and environmental heterogeneity among larks in the widespread, highly poltypic genus \textit{Eremophila}. Furthermore, I will consider whether rates of adaptive color evolution among species are associated with speciation rates among larks at the family level.} By considering both microevolutionary and macroevolutionary processes, I will infer the contribution of adaptive coloration towards diversification at multiple evolutionary scales.  

\bigskip

\noindent\textbf{Dissertation Research \& Context for Improvement}

\noindent My core dissertation research has three main components: (1) phylogeography and conservation genetics of New World larks in \textit{Eremophila}; (2) geographic variation in adaptive dorsal coloration and soil types in the western US; (3) temporal variation in adaptive dorsal coloration, soil color, and diet within the Imperial Valley of southeastern California. This DDIG extends my current research to include genetic and phenotypic analyses of Old World populations of \textit{Eremophila} to consider the effects of incomplete lineage sorting and gene flow in species tree inference and integrative species delimitation, analyze associations between geographic variation in dorsal coloration and environmental heterogeneity, and evaluate the macroevolutionary mode and tempo of adaptive coloration and diversification in Alaudidae. Thus, the research proposed here enhances my dissertation to forge conceptual and empirical links between infraspecific and interspecific evolutionary processes that drive diversification in larks.   

\bigskip

\noindent\textbf{Proposed Research Objectives}

\noindent My DDIG integrates species tree analyses through high-throughput sequencing, remote sensing data, spatial and spectral analyses of dorsal plumage, and phylogenetic comparative methods to study the evolutionary processes driving phenotypic variation and diversification at multiple evolutionary scales within larks (Alaudidae).

\bigskip

\hangindent=0.7cm

\begin{enumerate}[leftmargin=*,labelindent=0pt,itemindent=0pt,nolistsep,noitemsep,label=Objective \arabic*:]
\item Disentangle the effects of gene flow and incomplete lineage sorting in species tree inference and species delimitation in \textit{Eremophila}
\item Quantify background matching between dorsal color and substrate color among populations and species and test correlations between rates of dorsal color evolution are speciations rates across Alaudidae
\end{enumerate}

\bigskip

%Species are a fundamental unit of biodiversity across disciplines, while delimiting species and resolving their evolutionary relationships are fundamental and enduring goals within systematics \autocite{DeQueiroz2007,Mayr1982,Cracraft1983}. Accurate species-level phylogenies guide inferences regarding the evolutionary and ecological processes that shape taxonomic and phenotypic diversity over space and time. Comparative biogeographic analyses, which consider extant geographic distributions in a phylogenetic context, can reveal the history of vicariance and dispersal within a given lineage \autocite{Ronquist2011}. Large-scale geophysical events and long distance movements both contribute to extant spatial patterns of biodiversity; however, \textbf{the relative frequency of vicariance and dispersal events are often debated and vary among taxa} \autocite{Zink2000}. In order for dispersal to result in successful colonization, populations must persist in a new area, which may require adaptations to novel environmental conditions and divergent selective pressures \autocite{Lovette2002,Ghalambor2007}. Therefore, \textbf{lineage-specific differences in the ability to adapt to spatially and temporally variable environmental conditions can affect the frequency of successful colonization events and influence species' geographic distributions} \autocite{West-Eberhard1989,Agrawal2001,Price2003}.  
%
%Recent radiations with high levels of phenotypic diversity across large geographic expanses offer opportunities to explore how dispersal and developmental plasticity contribute to the generation of biodiversity. However, lineages that have undergone rapid diversification across large geographic expanses have been historically challenging for systematists. First, recent radiations exhibit copious incomplete lineage sorting, which hinders species tree inference, particularly when only a small number of loci are sampled \autocite{Maddison1997,Rosenberg2002,Doyle1992}. Second, widespread radiations often lack of tissues or other genetic resources for regions that are remote, unstable, or otherwise logistically difficult to sample, while incomplete sampling can produce spurious inferences of evolutionary history \autocite{Templeton2004}. Fortunately, recent advances in DNA sequencing technology offer solutions to both obstacles. High-throughput sequencing (HTS) produces orders of magnitude more genetic data, thereby providing unprecedented resolution of species trees and species-level relationships within recent radiations \autocite{Wagner2013,McCormack2013,Lemmon2013}. Moreover, certain HTS pipelines, particularly target capture methods, are particularly compatible with ancient DNA (aDNA) samples from natural history collections \autocite{Jones2015}. These technological advances 'unlock' natural history collections as invaluable genomic resources, which can improve geographic sampling\autocite{Bi2013}. Thus, HTS enables new avenues of systematic research that can provide insight into longstanding\textemdash yet previously intractable\textemdash lines of inquiry regarding recent, rapid, and geographically widespread episodes of diversification.  


\noindent\textbf{Background:}

\noindent\textit{Larks as a model system for studying adaptive coloration and diversification}

Alaudidae includes approximately 90 species of larks that predominantly occur in Africa and Eurasia and inhabit grasslands, savannahs, and deserts in temperate and tropical regions \autocite{Alstrom2013}. Larks forage on insects and seeds off the ground, and most species build cup nests in small depressions directly upon the substrate. Due to their terrestrial life history in areas with little to no vegetation, many larks have evolved cryptic coloration to avoid predation from visually-oriented avian predators. Because soil composition and color vary geographically, species and populations differ substantially in their dorsal coloration to match selective pressures imposed by the environment. Although many lark species exhibit intraspecific variation in cryptic coloration, geographic variation in dorsal plumage is most pronounced among larks in the genus \textit{Eremophila}, which is the only lineage of larks that has successfully colonized the Western Hemisphere. A multilocus phylogeny of Alaudidae exists to guide comparative analyses \autocite{Alstrom2013}; however, certain genera, such as \textit{Eremophila}, likely require substantial taxonomic revision to accurately reflect species-level diversity and valid inference of interspecific patterns of trait evolution \autocite{Alstrom2013,Drovetski2014,Mason2014}.

Few species are as widespread and polytypic as the Horned Lark (\textit{Eremophila alpestris}; Fig. 1). Current taxonomic treatments include over 40 subspecies on five different continents (e.g., \textcite{Clements2015}) that vary primarily in dorsal coloration and patterning, facial patterns, vocalizations, and body size \autocite{Behle1942,Cramp1985}. Geographic variation in dorsal coloration presumably reflects local adaptations in camouflage \autocite{Zink1986,Behle1942}. However, the degree of concordance between dorsal and soil coloration has never been empirically demonstrated; quantifying the prevalence of background matching and cryptic coloration within \textit{Eremophila} and other lark genera is a major component of my dissertation.

\bigskip


\begin{wrapfigure}{rh!}{0.5\textwidth}
\vspace{-24pt}
\includegraphics[width=0.5\textwidth]{Ealp_ssp_range.pdf}
\vspace{-12pt}
\caption{\textbf{Distribution of mtDNA phylogroups and subspecies of \textit{Eremophila alpestris}.} Seven mtDNA phylogroups are recognized within \textit{Eremophila} and are shown here with different colored outlines. Within each group, the fill color of each polygon represents a different subspecies. Subspecific diversity is highest in western North America, where over 20 subspecies have been described that vary substantially in dorsal coloration.}
\vspace{-8pt}
\end{wrapfigure}

\noindent\textit{Species tree inference and species delimitation within widespread, recent radiations}

Species are a fundamental unit of biodiversity across disciplines \autocite{DeQueiroz2007,Mayr1982,Cracraft1983}; accurate species-level phylogenies guide inferences regarding the evolutionary and ecological processes that shape taxonomic and phenotypic diversity over space and time. Recent radiations with high levels of phenotypic diversity across large geographic expanses, such as \textit{Eremophila} larks, offer opportunities to explore how dispersal and local adaptation contribute to biodiversity. However, episodes of rapid diversification across large geographic expanses have been historically challenging for systematists. First, recent radiations exhibit copious incomplete lineage sorting, which hinders species tree inference, particularly when only a small number of loci are sampled \autocite{Maddison1997,Rosenberg2002,Doyle1992}. Second, widespread radiations often lack of tissues or other genetic resources for regions that are remote, unstable, or otherwise logistically difficult to sample, while incomplete sampling can produce spurious inferences of evolutionary history \autocite{Templeton2004}. Fortunately, recent advances in DNA sequencing technology offer solutions to both obstacles. High-throughput sequencing (HTS) produces orders of magnitude more genetic data, thereby providing unprecedented resolution of species trees and species-level relationships within recent radiations \autocite{Wagner2013,McCormack2013,Lemmon2013}. Moreover, certain HTS pipelines, particularly target capture methods, are particularly compatible with ancient DNA (aDNA) samples from natural history collections \autocite{Jones2015}. These technological advances 'unlock' natural history collections as invaluable genomic resources, which can improve geographic sampling\autocite{Bi2013}. Thus, HTS enables new avenues of systematic research that can provide insight into longstanding\textemdash yet previously intractable\textemdash lines of inquiry regarding recent, rapid, and geographically widespread episodes of diversification.  

%\noindent\textit{Study System}
%
%
%\textit{Eremophila alpestris} is the only alaudid that has successfully colonized the Western Hemisphere and breeds in a remarkable variety of different habitats, including arid deserts, mesic meadows, high-elevation mountain clearings, and humid coastal areas \autocite{Beason1995}. Although they breed across a wide array of biomes, \textit{Eremophila} larks consistently breed in areas with little to no vegetation and nest on the ground. Because they inhabit open areas and frequently preyed upon by raptors \autocite{Beason1995}, larks rely heavily on crypsis and disruptive patterning for survival \autocite{Endler1978}. 

%\noindent\textit{Literature Review}
%
%Patterns of genetic variation differ dramatically among Holarctic birds. Some birds, such as \textit{Troglodytes} wrens exhibit deep phylogeographic structure between and within Old World and New World populations \textit{Drovetski2004}; in contrast, \textit{Acanthis} redpolls comprise a single, Holarctic gene pool that includes three currently recognized species \autocite{Mason2015}.
%
%In my dissertation research thus far, I have studied the phylogeography and spatial and temporal patterns of variation in dorsal coloration within a widespread, recent radiation of New World larks in the genus \textit{Eremophila}. Using mitochondrial DNA (mtDNA), I inferred that \textit{Eremophila} recently colonized the Channel Islands off of southern California (< 0.5 mya) and exhibits asymmetrical gene flow from the archipelago to the continent \autocite{Mason2014}. My collaborators sampled 291 individuals of \textit{Eremophila} from both Old World and New World populations and documented extensive geographically and phenotypically concordant structure using mtDNA \autocite{Drovetski2014}. However, \textcite{Drovetski2014} were unable to uncover robust support for the same topology using species-tree approaches with the single mtDNA gene region and two nuclear loci that they also sequenced. Therefore, it seems likely, but still unknown, that divergence in mtDNA may reflect multiple independent evolutionary lineages that merit species status. \textbf{Here, I propose to comprehensively sample populations and subspecies throughout the range of \textit{Eremophila} in combination with target capture many loci to accurately reconstruct the evolutionary history and perform integrative species delimitation within the genus. This phylogeny will then be used for historical biogeography and comparative analyses of variation in adaptive dorsal coloration and range sizes within \textit{Eremophila}}.

\bigskip

\noindent\textbf{Proposed Research: Methods and Preliminary Data}

\noindent\textit{Objective 1: Evaluate the effects of gene flow and incomplete lineage sorting in species tree inference and species delimitation in \textit{Eremophila}}

\noindent\underline{Predictions:}

\noindent\underline{Methods:} I will leverage high-throughput sequencing via target capture in order to sequence a panel loci for phylogenetic analyses. While target capture pipelines such as ultraconserved elements \autocite{McCormack2012a} and anchored phylogenomics \autocite{Lemmon2012} assess variation at thousands of loci, they are prohibitively expensive for projects that involve hundreds of individuals. Furthermore, patterns of coalescence can frequently be documented using far fewer loci, allowing more individuals and populations to be multiplexed and included within the budget of a given project. One promising method for acquiring an intermediate number of loci (i.e., 25 -- 100 genomic loci) via multiplexed target capture is sequence-capture using PCR-generated probes (hereafter SCPP; \cite{Penalba2014}). In brief, this method uses PCR products as baits to capture variation among closely related individuals by allowing fragmented DNA to hybridize to biotinylated beads. Because SCPP bypasses the need for generating probe kits through commercial providers, SCPP provides a more cost-effective method for capturing variation at loci that have primers already available for amplification. An additional benefit of using a target capture approach, such as SCPP, is that highly degraded aDNA samples, such as toe pads, can be easily incorporated along with fresh or frozen tissues \autocite{Bi2013}. In birds, numerous unlinked molecular markers used in phylogeography and population genetics have well-established universal primers \autocite{Kimball2009,Backstrom2008}. Here, I will generate probes from a panel of approximately 60 loci to capture variation among related individuals via SCPP. Once multiplexed libraries have been sequenced, raw reads will be filtered, demultiplexed, assembled into contigs, and aligned back to reference loci using the bioinformatics pipeline described by \textcite{Penalba2014}. Furthermore, because aDNA samples experience 

Studying the evolutionary history of geographic variation, speciation, and diversification of adaptive coloration in larks necessitates a robust phylogeny that depicts species-level relationships. Although phylogenetic analyses have confirmed the monophyly of \textit{Eremophila} with respect to other genera of larks in the family Alaudidae, \textit{E. alpestris} is paraphyletic, with respect to \textit{E. bilopha}\textemdash the only other currently recognized species in \textit{Eremophila} \autocite{Alstrom2013,Drovetski2014}. Previous studies have documented substantial structure in mtDNA among geographically disjunct subspecies of \textit{Eremophila} larks \autocite{Alstrom2013,Drovetski2005,Mason2014}, but similar patterns have not been recovered in the small number of nuclear loci that have been sequenced \autocite{Drovetski2014}. Based on molecular clocks, \textit{Eremophila} larks share a common ancestor estimated between 1.3\textendash 4 mya \autocite{Alstrom2013,Drovetski2014}, suggesting that diversification within the genus has transpired entirely within the Pliocene and Pleistocene epochs. Furthermore, Channel Islands populations of \textit{E. alpestris} off the coast of southern California exhibit substantial gene flow into continental populations \autocite{Mason2014}. Taken together, these inferences support the idea that recent long distance dispersal events have likely contributed substantially to Holarctic diversification in the genus. 

\bigskip

\noindent\textit{Objective 2: Analyze associations between geographic variation in dorsal coloration and environmental heterogeneity among Old and New World lineages in \textit{Eremophila}}

\noindent\underline{Predictions:} Geographic variation in dorsal coloration matches variation in substrate color among populations in \textit{Eremophila}. Furthermore, the amount of geographic variation among independent evolutionary lineages in \textit{Eremophila} is associated with the degree of environmental heterogeneity within each species' range.

\noindent\underline{Methods:} Here, I will use digital photography to quantify the hue and patterning of lark lineages within \textit{Eremophila}. This will be combined with remote sensing data from the Landsat 8 observation satellite to characterize the prevalence of background matching. 

\bigskip

\noindent\textit{Objective 3: Determine whether rates of dorsal color evolution are correlated with speciations rates within Alaudidae}

\noindent\underline{Predictions:} Rates of dorsal color evolution are positively correlated with speciation rates within Alaudidae. 

\noindent\underline{Methods:} This will use the MCC tree from \textcite{Alstrom2013} combined with my species delimitation results of \textit{Eremophila} to do comparative analyses of rates of trait evolution and diversification using BAMM. Using similar digital photography methods to objective 2, I will determine whether rates of trait evolution in dorsal color and patterning
are correlated with speciation rates among larks. 

\bigskip

\noindent\textbf{Feasibility \& Schedule}

\bigskip

\noindent\textbf{Significance}

\noindent\textit{Intellectual Merit}

\noindent\textit{Broader Impacts}

%\begin{itemize}[nolistsep,noitemsep]
%\item{Integrative research combining data from disparate sources such as ancient DNA from museums, remote sensing, digital photography.}
%\item{Takes advantage of understudied, yet well-suited, system to understand how local adaptations arise.}
%\item{Elucidate the relative roles of positive selection and introgression during adaptation.}
%\item{Sets the stage for future work on the functional genomics of adaptive coloration in Horned Larks}
%\end{itemize}


%\begin{itemize}[nolistsep,noitemsep]
%\item{Increase understanding of how land use affects native populations.}
%\item{Demonstrate the importance and utility of natural history collections.}
%\item{Graudate student (i.e., me) training.}
%\item{Undergraduate student training.}
%\item{Outreach to local schools in the Imperial Valley.}
%\end{itemize}

\newpage
\noindent\textbf{Literature Cited}

\vspace{-52pt}

\printbibliography

\end{document}