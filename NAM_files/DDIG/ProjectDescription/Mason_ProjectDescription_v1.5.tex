%% Must use XeLaTeX compiler in order to use Arial font

\documentclass[11pt]{article}
\usepackage{fontspec}
\setmainfont{Arial}
\usepackage[margin=1in]{geometry}
\usepackage{tabu}
%\usepackage{showframe}
\usepackage{enumitem}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage[font={small,bf}]{caption}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{array}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

%%%BibLaTeX

\usepackage[style=authoryear-comp,backend=bibtex,sorting=nyt, maxbibnames=2,maxcitenames=2,firstinits=true]{biblatex}%max authors?
\usepackage{xpatch}

\setlength\bibhang{0.5in}
\bibliography{2015DDIG}

\xpatchbibmacro{date+extrayear}{%
  \printtext[parens]%
}{%
  \setunit{\addperiod\space}%
  \printtext%
}{}{}
\renewbibmacro{in:}{}

%\nocite{*}

\renewbibmacro*{volume+number+eid}{%
  \printfield{volume}%
  \setunit*{\addcolon}
   \printfield{number}}
   
\DefineBibliographyStrings{english}{%Remove 'References' header
  references = {},
}

\DefineBibliographyStrings{french}{%
  pages = {},
}

\DefineBibliographyStrings{english}{%
  pages = {},
}
\DeclareFieldFormat[article]{title}{#1}
   
\begin{document}

%\pagenumbering{gobble}

\begin{center}
\textbf{Project Description}
\end{center}

\vspace{-6pt}

%\raggedright

\noindent\textbf{Introduction}

Species are a fundamental unit of biodiversity across disciplines, while delimiting species and resolving their evolutionary relationships are fundamental and enduring goals within systematics \autocite{DeQueiroz2007,Mayr1982,Cracraft1983}. Accurate species-level phylogenies guide inferences regarding the evolutionary and ecological processes that shape taxonomic and phenotypic diversity over space and time. Comparative biogeographic analyses, which consider extant geographic distributions in a phylogenetic context, can reveal the history of vicariance and dispersal within a given lineage \autocite{Ronquist2011}. Large-scale geophysical events and long distance movements both contribute to extant spatial patterns of biodiversity; however, \textbf{the relative frequency of vicariance and dispersal events are often debated and vary among taxa} \autocite{Zink2000}. In order for dispersal to result in successful colonization, populations must persist in a new area, which may require adaptations to novel environmental conditions and divergent selective pressures \autocite{Lovette2002,Ghalambor2007}. Therefore, \textbf{lineage-specific differences in the ability to adapt to spatially and temporally variable environmental conditions can affect the frequency of successful colonization events and influence species' geographic distributions} \autocite{West-Eberhard1989,Agrawal2001,Price2003}.  

Recent radiations with high levels of phenotypic diversity across large geographic expanses offer opportunities to explore how dispersal and developmental plasticity contribute to the generation of biodiversity. However, lineages that have undergone rapid diversification across large geographic expanses have been historically challenging for systematists. First, recent radiations exhibit copious incomplete lineage sorting, which hinders species tree inference, particularly when only a small number of loci are sampled \autocite{Maddison1997,Rosenberg2002,Doyle1992}. Second, widespread radiations often lack of tissues or other genetic resources for regions that are remote, unstable, or otherwise logistically difficult to sample, while incomplete sampling can produce spurious inferences of evolutionary history \autocite{Templeton2004}. Fortunately, recent advances in DNA sequencing technology offer solutions to both obstacles. High-throughput sequencing (HTS) produces orders of magnitude more genetic data, thereby providing unprecedented resolution of species trees and species-level relationships within recent radiations \autocite{Wagner2013,McCormack2013,Lemmon2013}. Moreover, certain HTS pipelines, particularly target capture methods, are particularly compatible with ancient DNA (aDNA) samples from natural history collections \autocite{Jones2015}. These technological advances 'unlock' natural history collections as invaluable genomic resources, which can improve geographic sampling\autocite{Bi2013}. Thus, HTS enables new avenues of systematic research that can provide insight into longstanding\textemdash yet previously intractable\textemdash lines of inquiry regarding recent, rapid, and geographically widespread episodes of diversification.  

Numerous species from distantly related taxa, including over 100 species of birds, have a widespread Holarctic distribution that spans North America, Europe, and Eurasia \autocite{Enghoff2005,Sanmartin2001}. Holarctic taxa, particularly birds, are well-suited for disentangling the complex interplay of glacial cycles, vicariance, dispersal, and developmental plasticity that shape geographic patterns of phenotypic and genetic diversity \autocite{Peters2014,Zink1995}. However, many Holarctic taxa are relatively understudied because of the relative difficulty in securing sufficient samples and the complex geological history of the region \autocite{Sanmartin2001}. My dissertation leverages Holarctic birds\textemdash specifically larks in the genus \textit{Eremophila}\textemdash in combination with technological advances to elucidate the evolutionary phenomena underlying rapid diversification over large geographic areas. \textbf{In this DDIG, I propose to improve upon my existing dissertation research on the systematics of New World \textit{Eremophila} by expanding my taxonomic sampling to include Old World lineages. In particular, funding will allow me to perform integrative (genetic + phenotypic data) species delimitation, infer species-level relationships, and subsequently test the role that dispersal and phenotypic plasticity in adaptive coloration have played in shaping extant distributions within \textit{Eremophila}.}

\bigskip

\noindent\textbf{Dissertation Research \& Context for Improvement}

\noindent My dissertation takes a multiscale approach to understand the ecological and evolutionary factors that have shaped genetic differentiation and phenotypic diversity among New World larks in the genus \textit{Eremophila}. There are three main components of my dissertation research: (1) phylogeography and conservation genetics of New World larks in \textit{Eremophila}; (2) geographic variation in adaptive dorsal coloration and substrate coloration in western US; (3) temporal variation in adaptive dorsal coloration, substrate coloration, and diet within the Imperial Valley of southeastern California. This DDIG extends my current research to include genetic and phenotypic analyses of Old World populations of \textit{Eremophila} to provide a more comprehensive, comparative study of taxonomic and phenotypic diversification within this recent, widespread genus. 

\bigskip

\noindent\textbf{Proposed Research Objectives}

\hangindent=0.7cm

\begin{enumerate}[leftmargin=*,labelindent=0pt,itemindent=0pt,nolistsep,noitemsep,label=\textit{Objective \arabic*:}]
\item Infer a species tree reflecting the relationships among lineages within the genus \textit{Eremophila}, and perform integrative species delimitation using both phenotypic and molecular data 
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0in,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Current taxonomy (2 species) is accurate} with respect to the number of independent evolutionary lineages in \textit{Eremophila}
	\item \textbf{Current taxonomy (2 species) is not accurate} with respect to the number of independent evolutionary lineages in \textit{Eremophila}
	\end{enumerate}
	\medskip
\item Infer the frequency of biogeographic modes of diversification within \textit{Eremophila}
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0pt,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Vicariance has predominated} throughout the biogeographic history of \textit{Eremophila}
	\item \textbf{Dispersal has predominated} throughout the biogeographic history of \textit{Eremophila}
	\end{enumerate}
	\medskip
\item Determine whether the degree of adaptive variation in dorsal coloration is related to range size within \textit{Eremophila} 
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0pt,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Degree of variation in dorsal plumage is not correlated with range size} among \textit{Eremophila} larks
	\item \textbf{Degree of variation in dorsal plumage is correlated with range size} among \textit{Eremophila} larks
	\end{enumerate}
\end{enumerate}

\bigskip

\noindent\textbf{Background}

\noindent\textit{Study System}

\noindent Few species are as widespread and polytypic as the Horned Lark (\textit{Eremophila alpestris}; Fig. 1). Current taxonomic treatments include over 40 subspecies on five different continents (e.g., \textcite{Clements2015}) that vary primarily in dorsal coloration and patterning, facial patterns, vocalizations, and body size \autocite{Behle1942,Cramp1985}. Although phylogenetic analyses have confirmed the monophyly of \textit{Eremophila} with respect to other genera of larks in the family Alaudidae, \textit{E. alpestris} is paraphyletic, such that certain subspecies of \textit{E. alpestris} are more closely related to \textit{E. bilopha}\textemdash the only other currently recognized species in \textit{Eremophila}\textemdash than they are to other subspecies of \textit{E. alpestris} \autocite{Alstrom2013,Drovetski2014}. Previous studies have documented substantial structure in mtDNA among geographically disjunct subspecies of \textit{Eremophila} larks \autocite{Alstrom2013,Drovetski2005,Mason2014}, but similar patterns have not been recovered in the small number of nuclear loci that have been sequenced \autocite{Drovetski2014}. Furthermore, various populations, such as \textit{E. a. enertera} in southern Baja California and \textit{E. a. peregrina} near Bogot\'{a}, Colombia have never been included in a phylogeographic study.  Therefore, we still do not know how many evolutionarily independent lineages exist within \textit{Eremophila} nor how they are related. 

Larks in the genus \textit{Eremophila} form a recent, widespread radiation. Based on molecular clocks, \textit{Eremophila} larks share a common ancestor estimated between 2.5\textendash 4 mya \autocite{Alstrom2013}, suggesting that  diversification within the genus has transpired entirely within the Pliocene and Pleistocene epochs. Furthermore, Channel Islands populations of \textit{E. alpestris} off the coast of southern California exhibit gene flow into continental populations \autocite{Mason2014}. Taken together, these inferences support the idea that long distance dispersal events have likely contributed substantially to diversification in the genus; however, the relative roles of dispersal and vicariance during the biogeographic history of \textit{Eremophila} have never been explicitly tested in a phylogenetic comparative framework.

\textit{Eremophila alpestris} is the only alaudid that has successfully colonized the Western Hemisphere and breeds in a remarkable variety of different habitats, including arid deserts, mesic meadows, high-elevation mountain clearings, and humid coastal areas \autocite{Beason1995}. Although they breed across a wide array of biomes, \textit{Eremophila} larks consistently breed in areas with little to no vegetation and nest on the ground. Because they inhabit open areas and frequently preyed upon by raptors \autocite{Beason1995}, larks rely heavily on crypsis and disruptive patterning for survival \autocite{Endler1978}. \autocite{Zink1986,Behle1942}

\bigskip

\noindent\textit{Literature Review}

Patterns of genetic variation differ dramatically among Holarctic birds. Some birds, such as \textit{Troglodytes} wrens exhibit deep phylogeographic structure between and within Old World and New World populations \textit{Drovetski2004}; in contrast, \textit{Acanthis} redpolls comprise a single, Holarctic gene pool that includes three currently recognized species \autocite{Mason2015}.

In my dissertation research thus far, I have studied the phylogeography and spatial and temporal patterns of variation in dorsal coloration within a widespread, recent radiation of New World larks in the genus \textit{Eremophila}. Using mitochondrial DNA (mtDNA), I inferred that \textit{Eremophila} recently colonized the Channel Islands off of southern California (< 0.5 mya) and exhibits asymmetrical gene flow from the archipelago to the continent \autocite{Mason2014}. My collaborators sampled 291 individuals of \textit{Eremophila} from both Old World and New World populations and documented extensive geographically and phenotypically concordant structure using mtDNA \autocite{Drovetski2014}. However, \textcite{Drovetski2014} were unable to uncover robust support for the same topology using species-tree approaches with the single mtDNA gene region and two nuclear loci that they also sequenced. Therefore, it seems likely, but still unknown, that divergence in mtDNA may reflect multiple independent evolutionary lineages that merit species status. \textbf{Here, I propose to comprehensively sample populations and subspecies throughout the range of \textit{Eremophila} in combination with target capture many loci to accurately reconstruct the evolutionary history and perform integrative species delimitation within the genus. This phylogeny will then be used for historical biogeography and comparative analyses of variation in adaptive dorsal coloration and range sizes within \textit{Eremophila}}.

\bigskip

\noindent\textbf{Proposed Research: Methods and Preliminary Data}

\noindent\textit{Objective 1}

\noindent\textit{Objective 2}

\noindent\textit{Objective 3}

\noindent\textbf{Feasibility \& Schedule}

\noindent\textbf{Significance}

\noindent\textit{Intellectual Merit}

\noindent\textit{Broader Impacts}

%\begin{itemize}[nolistsep,noitemsep]
%\item{Integrative research combining data from disparate sources such as ancient DNA from museums, remote sensing, digital photography.}
%\item{Takes advantage of understudied, yet well-suited, system to understand how local adaptations arise.}
%\item{Elucidate the relative roles of positive selection and introgression during adaptation.}
%\item{Sets the stage for future work on the functional genomics of adaptive coloration in Horned Larks}
%\end{itemize}


%\begin{itemize}[nolistsep,noitemsep]
%\item{Increase understanding of how land use affects native populations.}
%\item{Demonstrate the importance and utility of natural history collections.}
%\item{Graudate student (i.e., me) training.}
%\item{Undergraduate student training.}
%\item{Outreach to local schools in the Imperial Valley.}
%\end{itemize}

\newpage
\noindent\textbf{Literature Cited}

\vspace{-52pt}

\printbibliography

\end{document}