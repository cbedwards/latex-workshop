%% Must use XeLaTeX compiler in order to use Arial font

\documentclass[11pt]{article}
\usepackage{fontspec}
\setmainfont{Arial}
\usepackage[margin=1in]{geometry}
\usepackage{tabu}
%\usepackage{showframe}
\usepackage{enumitem}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage[font={small,bf}]{caption}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{array}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

%%%BibLaTeX

\usepackage[style=authoryear-comp,backend=bibtex,sorting=nyt, maxbibnames=2,maxcitenames=2,firstinits=true]{biblatex}%max authors?
\usepackage{xpatch}

\setlength\bibhang{0.5in}
\bibliography{2015DDIG}

\xpatchbibmacro{date+extrayear}{%
  \printtext[parens]%
}{%
  \setunit{\addperiod\space}%
  \printtext%
}{}{}
\renewbibmacro{in:}{}

%\nocite{*}

\renewbibmacro*{volume+number+eid}{%
  \printfield{volume}%
  \setunit*{\addcolon}
   \printfield{number}}
   
\DefineBibliographyStrings{english}{%Remove 'References' header
  references = {},
}

\DefineBibliographyStrings{french}{%
  pages = {},
}

\DefineBibliographyStrings{english}{%
  pages = {},
}
\DeclareFieldFormat[article]{title}{#1}
   
\begin{document}

%\pagenumbering{gobble}

\begin{center}
\textbf{Project Description}
\end{center}

\vspace{-6pt}

%\raggedright

\noindent\textbf{Introduction}

Species are a fundamental unit of biodiversity across disciplines, while delimiting species and resolving their evolutionary relationships are central and enduring goals within systematics \autocite{DeQueiroz2007,Mayr1982,Cracraft1983}. Under this broader goal, lineages that have undergone recent, rapid bouts of speciation accompanied by high levels of phenotypic diversity present a persistent challenge for systematists. Accurate species-level phylogenies are required to guide inferences regarding the evolutionary and ecological processes that shape biodiversity over time and space; nonetheless, \textbf{many recent radiations lack robust species-level phylogenies, which impedes our ability to gain biological inferences from comparative analyses}. Comparative phylogenetic analyses of extant geographic distributions and phenotypic diversity can reveal the biogeographic history of vicariance and dispersal within a given lineage and provide insight into patterns of trait evolution among taxa. Large-scale geophysical events and long distance movements have both contributed to extant spatial patterns of biodiversity; however, \textbf{the relative frequency of vicariance and dispersal events are often debated and vary among taxa} \autocite{Zink2000}. Furthermore, colonization events require populations to persist in a new area, which may require adapting to novel environmental conditions and selective pressures \autocite{,Ghalambor2007}. Therefore, \textbf{the capacity for organisms to adapt to spatially and temporally variable environmental conditions can affect the frequency of successful colonization events and influence species' distributions} \autocite{West-Eberhard1989,Agrawal2001}. 

Widespread, recent radiations present a multifaceted challenge for systematists. First, it is often difficult to resolve evolutionary relationships within recent radiations because of the high prevalence of incomplete lineage sorting \autocite{Rosenberg2002,Doyle1992}. High levels of ancestral polymorphism generate gene tree discordance and hinder accurate species tree inference \autocite{Maddison1997}. However, recent advances in high-throughput sequencing enable new avenues and methods of systematic research that were previously unfeasible \autocite{McCormack2013,Lemmon2013}. The immense amount of genetic data acquired through high-throughput sequencing can provide unprecedented resolution of species-level relationships within recent radiations \autocite{Wagner2013}. Second, widespread radiations are often difficult to sample extensively because of the lack of tissues or other genetic resources. However, certain high-throughput sequencing pipelines, specifically target capture methods, are particularly amenable to using ancient DNA (aDNA) from natural history collections \autocite{Jones2015}. These technological advances have 'unlocked' natural history collections as a repository of genomic resources \autocite{Bi2013}, which can boost the sampling of widespread taxa that include populations that have been vouchered in the past, but are difficult to sample today because they are in remote or unstable regions \autocite{Wandeler2007}.

In my dissertation research thus far, I have studied the phylogeography and spatial and temporal patterns of variation in dorsal coloration within a widespread, recent radiation of New World larks in the genus \textit{Eremophila}. Using mitochondrial DNA (mtDNA), I inferred that \textit{Eremophila} recently colonized the Channel Islands off of southern California (< 0.5 mya) and exhibits asymmetrical gene flow from the archipelago to the continent \autocite{Mason2014}. My collaborators sampled 291 individuals of \textit{Eremophila} from both Old World and New World populations and documented extensive geographically and phenotypically concordant structure using mtDNA \autocite{Drovetski2014}. However, \textcite{Drovetski2014} were unable to uncover robust support for the same topology using species-tree approaches with the single mtDNA gene region and two nuclear loci that they also sequenced. Therefore, it seems likely, but still unknown, that divergence in mtDNA may reflect multiple independent evolutionary lineages that merit species status. \textbf{Here, I propose to comprehensively sample populations and subspecies throughout the range of \textit{Eremophila} in combination with target capture many loci to accurately reconstruct the evolutionary history and perform integrative species delimitation within the genus. This phylogeny will then be used for historical biogeography and comparative analyses of variation in adaptive dorsal coloration and range sizes within \textit{Eremophila}}.

\bigskip

\noindent\textbf{Proposed Research Objectives}

\hangindent=0.7cm

\begin{enumerate}[leftmargin=*,labelindent=0pt,itemindent=0pt,nolistsep,noitemsep,label=\textit{Objective \arabic*:}]
\item Infer a species tree reflecting the relationships among lineages within the genus \textit{Eremophila}, and perform integrative species delimitation using both phenotypic and molecular data 
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0in,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Current taxonomy (2 species) is accurate} with respect to the number of independent evolutionary lineages in \textit{Eremophila}
	\item \textbf{Current taxonomy (2 species) is not accurate} with respect to the number of independent evolutionary lineages in \textit{Eremophila}
	\end{enumerate}
	\medskip
\item Infer the frequency of biogeographic modes of diversification within \textit{Eremophila}
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0pt,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Vicariance has predominated} throughout the biogeographic history of \textit{Eremophila}
	\item \textbf{Dispersal has predominated} throughout the biogeographic history of \textit{Eremophila}
	\end{enumerate}
	\medskip
\item Determine whether the degree of adaptive variation in dorsal coloration is related to range size within \textit{Eremophila} 
	\begin{enumerate}[leftmargin=*,labelindent=-0.5in,itemindent=0pt,nolistsep,noitemsep,label=\underline{Hypothesis \arabic*:}]
	\item \textbf{Degree of variation in dorsal plumage is not correlated with range size} among \textit{Eremophila} larks
	\item \textbf{Degree of variation in dorsal plumage is correlated with range size} among \textit{Eremophila} larks
	\end{enumerate}
\end{enumerate}

\bigskip

\noindent\textbf{Dissertation Research \& Context for Improvement}

\noindent My dissertation takes a multiscale approach to understand the ecological and evolutionary factors that have shaped genetic differentiation and phenotypic diversity within New World larks in the genus \textit{Eremophila}. There are three main components of my dissertation research: (1) phylogeography and conservation genetics of New World larks in \textit{Eremophila}; (2) geographic variation in dorsal coloration and substrate reflectance in western US; (3) temporal variation in cryptic coloration and diet within the Imperial Valley of southeastern California. This DDIG proposal extends my current research to include genetic and phenotypic analyses of Old World populations of \textit{Eremophila} to provide a more comprehensive understanding of taxonomic and phenotypic diversification within this recent, widespread genus. 

\bigskip

\noindent\textbf{Background}

\noindent\textit{Literature Review}

\noindent\textit{Study System}

\noindent\textbf{Proposed Research: Methods and Preliminary Data}

\noindent\textbf{Feasibility \& Schedule}

\noindent\textbf{Significance}

\noindent\textit{Intellectual Merit}

\noindent\textit{Broader Impacts}

%\begin{itemize}[nolistsep,noitemsep]
%\item{Integrative research combining data from disparate sources such as ancient DNA from museums, remote sensing, digital photography.}
%\item{Takes advantage of understudied, yet well-suited, system to understand how local adaptations arise.}
%\item{Elucidate the relative roles of positive selection and introgression during adaptation.}
%\item{Sets the stage for future work on the functional genomics of adaptive coloration in Horned Larks}
%\end{itemize}


%\begin{itemize}[nolistsep,noitemsep]
%\item{Increase understanding of how land use affects native populations.}
%\item{Demonstrate the importance and utility of natural history collections.}
%\item{Graudate student (i.e., me) training.}
%\item{Undergraduate student training.}
%\item{Outreach to local schools in the Imperial Valley.}
%\end{itemize}

\newpage
\noindent\textbf{Literature Cited}

\vspace{-52pt}

\printbibliography

\end{document}