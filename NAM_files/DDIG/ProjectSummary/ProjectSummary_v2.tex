%% Must use XeLaTeX compiler in order to use Arial font

\documentclass[10pt]{article}
\usepackage{fontspec}
\setmainfont{Arial}
\usepackage[margin=1in]{geometry}
\usepackage{tabu}
%\usepackage{showframe}
\usepackage{enumitem}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage[font={small,bf}]{caption}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{array}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

%%%BibLaTeX

\usepackage[style=authoryear-comp,backend=bibtex,sorting=nyt, maxbibnames=2,maxcitenames=2,firstinits=true]{biblatex}%max authors?
\usepackage{xpatch}

\setlength\bibhang{0.5in}
\bibliography{DDIG}

\xpatchbibmacro{date+extrayear}{%
  \printtext[parens]%
}{%
  \setunit{\addperiod\space}%
  \printtext%
}{}{}
\renewbibmacro{in:}{}

%\nocite{*}

\renewbibmacro*{volume+number+eid}{%
  \printfield{volume}%
  \setunit*{\addcolon}
   \printfield{number}}
   
\DefineBibliographyStrings{english}{%Remove 'References' header
  references = {},
}

\DefineBibliographyStrings{french}{%
  pages = {},
}

\DefineBibliographyStrings{english}{%
  pages = {},
}
\DeclareFieldFormat[article]{title}{#1}
   
\begin{document}

\linespread{1.03}

\pagenumbering{gobble}

\begin{center}
\textbf{Project Summary}
\end{center}

%\raggedright

%\noindent\textbf{Introduction}

\vspace{-6pt}

\noindent\textbf{Overview}:  This DDIG proposes an integrative study of phenotypic and genetic differentiation within a recent, widespread, and highly polytypic lineage of larks in the genus \textit{Eremophila}. The PIs will leverage traditionally disparate data sources, including multiple forms of remote sensing data, digital photography, and ancient DNA from museum toepads via Sequence Capture using PCR-generated Probes (SCPP), to delimit species and undertake comparative analyses of cryptic coloration among populations and newly delimited species. Using SCPP, the PIs will generate a panel of 60 loci for 288 individuals, including the mitochondrial genome, sex-linked markers, and autosomal introns and exons, for phylogeographic analyses and integrative species delimitation alongside phenotypic variation in song, plumage, and morphology. Upon delimiting species and resolving relationships within the genus, the PIs will use the revised taxonomy and corresponding species tree to quantify the prevalence of geographic variation in cryptic coloration\textemdash or the degree of background matching between dorsal plumage and corresponding substrates\textemdash among populations and species. Finally, the PIs will test whether the amount of variation in cryptic coloration is correlated with abiotic niche breadth among species in \textit{Eremophila}, which would suggest that evolvability of cryptic coloration plays an important role facilitating range expansions into different habitats. 

%In sum, this DDIG has three goals: (1) delimit species limits and resolve the evolutionary relationships within \textit{Eremophila}, a widespread, diverse genus of larks; (2) quantify variation in cryptic coloration among populations and species within \textit{Eremophila}; and (3) test for a correlation between the degree of variation in cryptic coloration and niche breadth among species in \textit{Eremophila}. This DDIG forges a conceptual link between the existing population-level and family-level work of the Co-PI's dissertation, which focuses on spatial and temporal variation in dorsal plumage among larks in the western United States and family-level comparative analyses of cryptic coloration among larks (Alaudidae).

%When species experience changes to their native habitat, they must adapt, acclimate, disperse, or face extinction. Understanding the source of genetic variation and the evolutionary processes that facilitate adaption or acclimation are fundamental goals of evolutionary biology. Moreover, characterizing selective and demographic responses to novel selection pressures is an essential prerequisite for mitigating biodiversity loss amid human population growth. Here, the PIs integrate high-throughput sequencing, museum collections, analyses of plumage color and patterning, and remote sensing data to study the evolutionary processes underlying an instance of rapid adaptation in response to anthropogenic change. The study focuses on the Imperial Valley in southern California, where the rise of agriculture has imposed novel selective pressures on native species. Concurrent with the spread of agriculture in the southern Colorado Desert, Horned Larks (\textit{Eremophila alpestris}) have become darker over the last century. Horned Larks are granivorous songbirds that prefer areas with little to no vegetation and likely evolved darker coloration as camouflage to avoid avian predators amid irrigated, darker fields. By comparing genetic and phenotypic variation among ancestral and extant populations, the PIs will test for signatures of positive selection, gene flow, and demographic change that drive rapid adaptation. The PIs will determine whether the melanic individuals arose through selection acting on standing variation within the Imperial Valley, or are the product of introgression from genetically distinct populations. These comparisons will be done within the context of the graduate student PI's core dissertation work on range-wide phylogeography and species delimitation within Horned Larks.

\smallskip

\noindent\textbf{Intellectual Merit}: Studies of discrete variation in cryptic coloration have provided some of our most celebrated examples of natural selection. Yet, we still know relatively little about the evolutionary and ecological dynamics underlying more complex, subtle, and continuous patterns of geographic variation in adaptive coloration. This study integrates traditionally disparate sources of data, including high-throughput sequencing, remote sensing satellite data, and spatial and spectral analyses of coloration, to reveal conceptual and empirical links between genetic and phenotypic diversification at the population and species level. Identifying similarities and differences between microevolutionary processes among individuals and populations versus macroevolutionary processes among species and lineages is an longstanding goal within evolutionary biology; however, studies that span this diversity continuum are relatively scarce. Furthermore, widespread, recent radiations have persistently challenged systematists and taxonomists. By leveraging sequence capture methods in combination with ancient DNA from museum specimens, this research will provide a case study for incorporating natural history collections into phylogeographic analyses of widespread lineages in the genomics era. This DDIG will objectively evaluate evidence for alternative hypotheses regarding the number of species within \textit{Eremophila}, thereby improving alpha taxonomy by delimiting new species and a providing an improved phylogenetic framework for comparative analyses among larks. Finally, the analyses proposed here will provide conceptual and empirical groundwork for future research on the genetic architecture underlying continuous variation in cryptic coloration within larks.

\smallskip

\noindent\textbf{Broader Impacts}: This DDIG will train the graduate student Co-PI in skills and techniques that directly align with his professional goals. The PIs will recruit underrepresented demographics as research assistants and mentees to help complete the proposed research and will publish peer-reviewed articles regarding the benefits of experiential learning for underrepresented STEM demographics. The PIs will disseminate findings to both scientific and non-scientific audiences by leveraging their affiliations with the Cornell Lab of Ornithology (CLO) to engage public interest groups, such as bird watchers, through local lectures and popular science articles. In collaboration with the education department at the CLO, the co-PI will generate interactive, online resources to educate the public about natural selection and adaptation by drawing on his work on cryptic coloration in larks. The co-PI will forge and maintain multiple international collaborations to collect and acquire vouchered specimens for the project. The proposed research also will improve our understanding of the evolutionary distinctiveness of multiple threatened lineages. Finally, the co-PI will continue serving on service-based committees for the American Ornithologists' Union (AOU) to submit revisions for North American avian taxonomy based on the proposed research and promote undergraduate and graduate student activities and involvement at future AOU meetings. 

%
%\begin{itemize}[nolistsep,noitemsep]
%\item{Integrative research combining data from disparate sources such as ancient DNA from museums, remote sensing, digital photography.}
%\item{Takes advantage of understudied, yet well-suited, system to understand how local adaptations arise.}
%\item{Elucidate the relative roles of positive selection and introgression during adaptation.}
%\item{Sets the stage for future work on the functional genomics of adaptive coloration in Horned Larks}
%\end{itemize}


%\begin{itemize}[nolistsep,noitemsep]
%\item{Increase understanding of how land use affects native populations.}
%\item{Demonstrate the importance and utility of natural history collections.}
%\item{Graudate student (i.e., me) training.}
%\item{Undergraduate student training.}
%\item{Outreach to local schools in the Imperial Valley.}
%\end{itemize}

\end{document}