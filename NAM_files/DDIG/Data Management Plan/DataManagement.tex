%% Must use XeLaTeX compiler in order to use Arial font

\documentclass[10pt]{article}
\usepackage{fontspec}
\setmainfont{Arial}
\usepackage[margin=1in]{geometry}
\usepackage{tabu}
%\usepackage{showframe}
\usepackage{enumitem}
\usepackage[hidelinks]{hyperref}
\usepackage{wrapfig}
\usepackage[font={small,bf}]{caption}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{array}
\usepackage{longtable}

\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;} 

%%%BibLaTeX

\usepackage[style=authoryear-comp,backend=bibtex,sorting=nyt, maxbibnames=2,maxcitenames=2,firstinits=true]{biblatex}%max authors?
\usepackage{xpatch}

\setlength\bibhang{0.5in}
\bibliography{DDIG}

\xpatchbibmacro{date+extrayear}{%
  \printtext[parens]%
}{%
  \setunit{\addperiod\space}%
  \printtext%
}{}{}
\renewbibmacro{in:}{}

%\nocite{*}

\renewbibmacro*{volume+number+eid}{%
  \printfield{volume}%
  \setunit*{\addcolon}
   \printfield{number}}
   
\DefineBibliographyStrings{english}{%Remove 'References' header
  references = {},
}

\DefineBibliographyStrings{french}{%
  pages = {},
}

\DefineBibliographyStrings{english}{%
  pages = {},
}
\DeclareFieldFormat[article]{title}{#1}
   
\begin{document}

\pagenumbering{gobble}

\begin{center}
\textbf{Data Management Plan}
\end{center}

%\raggedright

\noindent \textbf{Description:} 

\noindent This DDIG project will produce four types of data:

\begin{enumerate}[nolistsep,leftmargin=*]
\item \textit{Voucher specimens of birds, tissue samples, and related data:} approximately 60 specimens and tissues have been collected by mistnet and shotgun from Imperial County, California; Boyac\'{a}, Colombia; Oaxaca, Mexico. These specimens will be used for phenotypic analyses of dorsal coloration and tissues will be used for genetic analyses. Tags for museum specimens will include metadata about the locality and timing of collection, as well as additional information about weight, fat content, and other data.
\item \textit{DNA sequence loci and reads:} this project will produce DNA sequence data for approximately 60 loci from approximately 288 individuals, as well as raw Illumina reads.
\item \textit{Phenotypic data:} this project will produce digital phenotypic data that describe variation in morphology, song, and plumage among hundreds of individuals.
%\item \textit{Remote sensing data:} this project will produce raw and processed remote sensing data used to assess change in the desert substrate.
\item \textit{Phylogenetic and population genetic data:} this project will produce gene trees for approximately 60 loci from 288 individuals as well as input files for programs, such as Structure. Input files will be shared on open-access data repositories in order to ensure that results are reproducible. 
\end{enumerate}

\bigskip

\noindent \textbf{Content and Format:} 
\begin{enumerate}[nolistsep,leftmargin=*]
\item \textit{Voucher specimens of birds, tissue samples, and related data:} Specimens collected in the field include associated data entered in excel with the following column headings: field collection number, genus, species, country, province, locality, elevation, latitude, longitude, natural history notes, date and time, collector, sex, soft part colors, mass, wing span, total length, mass, skull ossification, and fat content. All specimens associated with this dissertation and their corresponding metadata will be curated by the Cornell University Museum of Vertebrates and tissues will be stored in an ultracold freezer. Tissue and specimen information will be shared via public portals, such as Arctos and VertNet.
\item \textit{DNA sequence loci and reads:} DNA sequence data from the SCPP pipeline, including raw and assembled reads in FASTA format, will be archived in public databases following standard formats and will include species name and museum voucher number. All bioinformatic scripts used to filter and assemble loci will be included. Raw Illumina reads will be archived in the NCBI Short Read Archive and assembled loci will be archived in GenBank.
\item \textit{Phenotypic data:} Phenotypic (coloration, morphology, and song) data will be entered in Excel and stored as comma-separated values (CSV) for importation into R for statistical analyses. CSV files will include the museum voucher number as a unique identifier for each individual, followed by the various coloration data collected, including brightness, chroma, hue disparity, and color volume. The headers for each individual will be the same in this file and all subsequent files to facilitate reproduction and future use. All R scripts used to perform statistical analyses of phenotypic data will also be included. Information on the content and format of these file will be included in a README document that will be housed on a public data repository, such as Dryad. 
%\item \textit{Remote sensing data:} Raw and processed remote sensing data from AVIRIS will be stores as comma-separated values. This CSV file will include the latitude and longitude associated with each remote sensing data point as well as the spectral reflectance profile taken from a given coordinate. All programming scripts used to process and analyze remote sensing data will be included as well. Information on the content and format of these files will be included in a README document that will be housed on a public data archive, such as Dryad.
\item \textit{Phylogenetic and population genetic data:} Input files for population genetic and phylogenetic analyses will be included as well as downstream output, such as gene trees (Newick and nexus format). The labels identifying each individual will be the same in these files as they are for the phenotypic data. Data for each locus will be stored as a separate NEXUS file and as a PHYLIP file. The name of each NEXUS and PHYLIP file will match the information in the NCBI accession to ease interpretation. Additional population genetic input files, such as the input file for STRUCTURE, will also be included. Information on the content and format of these files will be included in a README document that will be stored in a public data repository, such as Dryad. 
\end{enumerate}

\bigskip

\noindent \textbf{Protection:} 

\noindent My research is limited to non-human subjects. Thus, I do not have any issues of privacy or confidentiality. Data will be protected by archiving them via CrashPlan, an online backup service that continually backs up data. I will also store data on three computers within the Lovette lab, an external departmental hard drive, and a personal backup drive. 
\bigskip

\newpage

\noindent \textbf{Access:} 

\noindent I will make data from this project available for any scientific and professional uses. Upon publication, all data become "open access" (e.g. in an online repository). If I have not published these data by the end of the DDIG timeframe, I will make them publicly available through an online repository. CSV files of phenotypic and remote sensing data in addition to input files for phylogenetic and population genetic analyses will be made available through the Dryad data repository (http://datadryad.org/). In agreement with the terms set by Dryad, these files will be shared under the terms of the Creative Commons Zero (CC0) waiver. I have not and will not ask for co-authorship as a condition for sharing data. I only request that when data are used, the paper in which they originally appeared should be cited as the source of the data. For any data that are immediately made available to the scientific community (e.g. specimens and associated metadata), I request that the relevant museum or repository is acknowledged (e.g. Cornell University Museum of Vertebrates).

\bigskip

\noindent \textbf{Preservation and Transfer of Responsibilities:} 

\noindent I will guarantee long-term continuation of preservation, oversight and access to all data associated with this grant. To accomplish this, most data types will be archived in public databases (e.g. NCBI databases for genetic data; Dryad for color and remote sensing data, etc.) upon publication. Any remaining data will be deposited in Cornell University Library's institutional repository, eCommons (http://ecommons.library.cornell.edu/), for preservation and access, where data sets will be available via the World Wide Web without restriction. eCommons provides each item with a persistent URL and is committed to preserving the binary form of the digital object.

\end{document}